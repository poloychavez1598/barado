
@php
    date_default_timezone_set('Asia/Manila');
    $dateNow = \Carbon\Carbon::parse(date('Y-m-d H:i:s'));
    $date = \Carbon\Carbon::parse(date('2021-10-17 23:59:00'));
    
    $setDate = $date->toDateString();
    echo $setDate . "<br><br>";
    //$addHour = $dateNow->addHour(12);
    //$subHour = $dateNow->subHour(24);
    echo "date now: ".$dateNow."<br><br>";
    echo "date: ".$date."<br><br>";
    //echo "add hr: ".$addHour."<br><br>";
    //echo "sub hr: ".$subHour."<br><br>";
    echo "diff in hr: ".$dateNow->diffInHours($date)."<br><br>";
    //echo $dateNow - $date."<br><br>";
    //echo $dateNow->subHour(24)->toDateString()."<br><br>";

    if ($dateNow->diffInHours($date) < 24) {
        echo $date->diffForHumans();
    }
    elseif ($dateNow->diffInHours($date) < 48) {
        if ($dateNow->subHour(24)->toDateString() == $setDate) {
            echo 'Yesterday '.date('h:i A', strtotime($date));
        }
        else {
            echo $date->toDayDateTimeString();
        }
    }
    else {
        echo $date->toDayDateTimeString();
    }
@endphp