-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 13, 2021 at 12:52 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `free_dikit`
--

-- --------------------------------------------------------

--
-- Table structure for table `child_comments`
--

CREATE TABLE `child_comments` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_comments_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `child_comments`
--

INSERT INTO `child_comments` (`id`, `parent_comments_id`, `user_id`, `comment`, `created_at`, `updated_at`) VALUES
('1_0000-0120_2021-09-26 00:00:00_child_0000-0120_2021-09-29 00:58:53', '1_0000-0120_2021-09-26 00:00:00_parent_0000-0110_2021-09-29 00:02:34', '0000-0120', 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Aperiam officia, mollitia aliquam cumque enim doloremque ducimus nostrum culpa obcaecati earum, facilis nemo vel tempora cupiditate eaque, atque expedita quod beatae?', '2021-09-28 16:58:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2021_09_03_160935_create_posts_table', 1),
(2, '2021_09_28_151918_create_parent_comments_table', 2),
(3, '2021_09_28_152001_create_child_comments_table', 3),
(4, '2021_10_19_191531_users', 4),
(5, '2021_10_19_195757_change_datetime_last_session_data_type', 5);

-- --------------------------------------------------------

--
-- Table structure for table `parent_comments`
--

CREATE TABLE `parent_comments` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `posts_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `parent_comments`
--

INSERT INTO `parent_comments` (`id`, `posts_id`, `user_id`, `comment`, `created_at`, `updated_at`) VALUES
('1_0000-0120_2021-09-26 00:00:00_parent_0000-0110_2021-09-29 00:02:34', '1_0000-0120_2021-09-26 00:00:00', '0000-0110', 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Aperiam officia, mollitia aliquam cumque enim doloremque ducimus nostrum culpa obcaecati earum, facilis nemo vel tempora cupiditate eaque, atque expedita quod beatae?', '2021-09-25 16:02:34', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_type_id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_content` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_posted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `post_type_id`, `user_id`, `text_content`, `file_content`, `time_posted`) VALUES
('1_0000-0120_2021-09-26 00:00:00', 1, '0000-0120', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut, rerum. At similique explicabo, cum quidem corporis neque hic eos asperiores quia, molestias vero vel dolor, dolores quos sint sequi natus.', 'post_files/images/wallpaper-hd.jpg', '2021-09-26 00:00:00'),
('1_2021-10-21&02:36:22_yangwenli@gmail.com_2021-11-07 22:35:51', 1, '2021-10-21&02:36:22_yangwenli@gmail.com', 'example\\t\\texample1\\r\\nexample\\r\\n\\r\\nexample', 'post_files/images/1_2021-10-21&02:36:22_yangwenli@gmail.com_2021-11-07 22:35:51_external-content.duckduckgo.com.png', '2021-11-07 22:35:51'),
('1_2021-10-21&02:36:22_yangwenli@gmail.com_2021-11-07 22:38:15', 1, '2021-10-21&02:36:22_yangwenli@gmail.com', 'check\\tasdasd\\t123456\\t212456\\r\\nasdffhhhhhh\\t24234\\t5345677', '', '2021-11-07 22:38:15'),
('1_2021-10-22&00:15:47_reinhard123@gmail.com_2021-11-10 01:01:18', 1, '2021-10-22&00:15:47_reinhard123@gmail.com', '', 'post_files/documents/1_2021-10-22&00:15:47_reinhard123@gmail.com_2021-11-10 01:01:18_Undergrad-PRF-DPP-Wordversion.docx', '2021-11-10 01:01:18'),
('1_2021-10-22&00:15:47_reinhard123@gmail.com_2021-11-13 19:33:47', 1, '2021-10-22&00:15:47_reinhard123@gmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam felis arcu, faucibus in dapibus a, molestie sit amet ipsum. Quisque feugiat vulputate elit. Proin tortor libero, blandit vel nulla sed, congue vulputate lacus. Sed lobortis dictum congue. In bibendum suscipit malesuada. Pellentesque pharetra ultricies quam at facilisis. In hac habitasse platea dictumst. Aliquam quis arcu suscipit, pharetra nibh eu, feugiat sem. Fusce tempus rutrum hendrerit. Aliquam erat volutpat.\\r\\n\\r\\nMaecenas ut sem in nunc aliquam tempus. Quisque ut bibendum ante. Vivamus mattis dictum justo, ut dapibus lorem mattis et. Nam tristique elit eu mattis ultricies. Curabitur in purus massa. Nullam eget velit ut dolor mattis euismod. Etiam aliquet turpis mi, eu tristique lectus rutrum pretium. Vestibulum maximus ante at felis viverra, ac pharetra enim accumsan. Pellentesque feugiat nibh vel orci convallis pretium. Ut aliquet finibus mattis.\\r\\n\\r\\nIn eu scelerisque sapien. Ut volutpat massa et arcu blandit, a sollicitudin nisi ullamcorper. Etiam porttitor pulvinar tortor, nec consectetur odio scelerisque interdum. Quisque sed neque lacus. Nulla facilisi. Sed eget ligula pharetra, facilisis libero maximus, cursus mauris. Morbi odio sem, consectetur non odio id, aliquet tincidunt augue.', 'post_files/documents/1_2021-10-22&00:15:47_reinhard123@gmail.com_2021-11-13 19:33:47_Reading4_andreassen2015.pdf', '2021-11-13 19:33:47'),
('2_0000-0110_2021-09-17 21:58:16', 2, '0000-0110', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut, rerum. At similique explicabo, cum quidem corporis neque hic eos asperiores quia, molestias vero vel dolor, dolores quos sint sequi natus.\\n\\nLorem ipsum dolor sit amet consectetur adipisicing elit. Ut, rerum. At similique explicabo, cum quidem corporis neque hic eos asperiores quia, molestias vero vel dolor, dolores quos sint sequi natus.\\n\\nLorem ipsum dolor sit amet consectetur adipisicing elit. Ut, rerum. At similique explicabo, cum quidem corporis neque hic eos asperiores quia, molestias vero vel dolor, dolores quos sint sequi natus.', '', '2021-09-17 21:58:16'),
('2_2021-10-20&03:36:01_sasuke@gmail.com_2021-11-13 19:51:29', 2, '2021-10-20&03:36:01_sasuke@gmail.com', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut, rerum. At similique explicabo, cum quidem corporis neque hic eos asperiores quia, molestias vero vel dolor, dolores quos sint sequi natus.', 'post_files/images/2_2021-10-20&03:36:01_sasuke@gmail.com_2021-11-13 19:51:29_walk_5.png', '2021-11-13 19:51:29'),
('3_0000-0120_2021-09-27 02:50:53', 3, '0000-0120', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut, rerum. At similique explicabo, cum quidem corporis neque hic eos asperiores quia, molestias vero vel dolor, dolores quos sint sequi natus.', '', '2021-09-27 02:50:53'),
('3_2021-10-20&03:36:01_sasuke@gmail.com_2021-11-13 19:47:49', 3, '2021-10-20&03:36:01_sasuke@gmail.com', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut, rerum. At similique explicabo, cum quidem corporis neque hic eos asperiores quia, molestias vero vel dolor, dolores quos sint sequi natus.\\r\\n\\r\\nLorem ipsum dolor sit amet consectetur adipisicing elit. Ut, rerum. At similique explicabo, cum quidem corporis neque hic eos asperiores quia, molestias vero vel dolor, dolores quos sint sequi natus.\\r\\n\\r\\nLorem ipsum dolor sit amet consectetur adipisicing elit. Ut, rerum. At similique explicabo, cum quidem corporis neque hic eos asperiores quia, molestias vero vel dolor, dolores quos sint sequi natus.', 'post_files/documents/3_2021-10-20&03:36:01_sasuke@gmail.com_2021-11-13 19:47:49_Reading1_ee_2016_03_Mashingaidze.pdf', '2021-11-13 19:47:49');

-- --------------------------------------------------------

--
-- Table structure for table `post_type`
--

CREATE TABLE `post_type` (
  `id` int(10) UNSIGNED NOT NULL,
  `postType_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post_type`
--

INSERT INTO `post_type` (`id`, `postType_name`) VALUES
(1, 'POST'),
(2, 'ANNOUNCEMENT'),
(3, 'NEWS');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `accType` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `datetime_last_session` datetime DEFAULT NULL,
  `isActive` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fname`, `lname`, `password`, `email`, `accType`, `datetime_last_session`, `isActive`, `created_at`, `updated_at`) VALUES
('0000-0000', 'Super User', 'Admin', '$2y$10$dZPHA2HhO9fnwtwA4rvvfOE.CBjS7wYqM5yTnAvQXG3.rsRNHrEp6', 'sample@email.com', 'su_admin', '2021-08-02 23:24:24', 1, NULL, NULL),
('0000-0010', 'Admin1', 'Admin1', '$2y$10$dZPHA2HhO9fnwtwA4rvvfOE.CBjS7wYqM5yTnAvQXG3.rsRNHrEp6', 'wala@gmail.com', 'admin', '2021-07-26 13:59:39', 1, NULL, NULL),
('0000-0100', 'User1', 'User1', '$2y$10$dZPHA2HhO9fnwtwA4rvvfOE.CBjS7wYqM5yTnAvQXG3.rsRNHrEp6', 'wala1@gmail.com', 'user', '2021-06-24 23:46:13', 1, NULL, NULL),
('0000-0110', 'Lin Ken', 'Park', '$2y$10$J.4E2fdfMGIIBCGPgaX3.eM4odrMrZVBaa2P9umgYRYBPHg7wIFhK', '0ynM0gCG5f@gmail.com', 'user', '2021-09-14 15:15:36', 1, NULL, NULL),
('0000-0120', 'Kenith', 'Iran', '$2y$10$QoK9A5jWBNDtrs9Nc9LCZOcfsUzLP89kmBMjqJMYXXsAAwY1/nnei', 'GpoU9wBF7f@gmail.com', 'user', '2021-09-14 15:18:47', 1, NULL, NULL),
('2021-10-20&03:36:01_sasuke@gmail.com', 'Sasuke', 'Uchiha', '$2y$10$Ra7AoQo44Qqi6jf0noQBku3UN5PEUI.94/KeGYMqqyLbQtKcXijqW', 'sasuke@gmail.com', 'user', '2021-10-20 03:36:01', 1, '2021-10-19 19:36:01', '2021-10-19 19:36:01'),
('2021-10-21&02:36:22_yangwenli@gmail.com', 'Yang', 'Wenli', '$2y$10$pAKlr/WsgYcALspsSlnSre8lg/9xW1qzSaJKiIBg9f.zRlAVOg6Ue', 'yangwenli@gmail.com', 'user', NULL, 1, '2021-10-20 18:36:22', '2021-10-20 18:36:22'),
('2021-10-22&00:15:47_reinhard123@gmail.com', 'Reinhard', 'Lohengramm', '$2y$10$RWJoUO3sICM9kZt2Jy2PbOXUYUEEZEAN95reToIX3vcZHxGzISChe', 'reinhard123@gmail.com', 'user', NULL, 1, '2021-10-21 16:15:47', '2021-10-21 16:15:47');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `child_comments`
--
ALTER TABLE `child_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `child_comments_parent_comments_id_foreign` (`parent_comments_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parent_comments`
--
ALTER TABLE `parent_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_comments_posts_id_foreign` (`posts_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_post_type_id_foreign` (`post_type_id`),
  ADD KEY `post_user_id_foreign` (`user_id`) USING BTREE;

--
-- Indexes for table `post_type`
--
ALTER TABLE `post_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `child_comments`
--
ALTER TABLE `child_comments`
  ADD CONSTRAINT `child_comments_parent_comments_id_foreign` FOREIGN KEY (`parent_comments_id`) REFERENCES `parent_comments` (`id`);

--
-- Constraints for table `parent_comments`
--
ALTER TABLE `parent_comments`
  ADD CONSTRAINT `parent_comments_posts_id_foreign` FOREIGN KEY (`posts_id`) REFERENCES `posts` (`id`);

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_post_type_id_foreign` FOREIGN KEY (`post_type_id`) REFERENCES `post_type` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
