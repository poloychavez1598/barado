<?php

namespace App\Http\Controllers;

use App\Models\ChildComments;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

class ChildCommentsController extends Controller
{
    public function getChildComments($parent_comments_id) {
        $childComments = ChildComments::where('parent_comments_id', $parent_comments_id)->get();

        return $childComments;
    }

    public function countChildComments($parent_comments_id) {
        $childComments = ChildComments::where('parent_comments_id', $parent_comments_id)->count();

        return $childComments;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\child_comments  $child_comments
     * @return \Illuminate\Http\Response
     */
    public function show(child_comments $child_comments)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\child_comments  $child_comments
     * @return \Illuminate\Http\Response
     */
    public function edit(child_comments $child_comments)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\child_comments  $child_comments
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, child_comments $child_comments)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\child_comments  $child_comments
     * @return \Illuminate\Http\Response
     */
    public function destroy(child_comments $child_comments)
    {
        //
    }
}
