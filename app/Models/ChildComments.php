<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChildComments extends Model
{
    use HasFactory;

    protected $table = 'child_comments';

    public $incrementing = false;

    protected $dates = ['created_at', 'updated_at'];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function parentComments() {
        return $this->belongsTo(ParentComments::class);
    }
}
