<div class="container-fluid shadow-sm mb-3 bg-white sticky-top overflow-hidden">
    
    <div class="container py-1">
        <div class="row">

            <div class="col-md-8 d-flex justify-content-sm-start">
                <div class="my-auto mx-2">
                    <a href="/main" class="">
                        <img src="{{ asset('image/logo_wallpaper/cic-lc-logo.jpg') }}" width="40" height="40">
                    </a>
                </div>
                <div class="my-auto">
                    <form class="input-group">
                        <input class="form-control" type="text" name="srchInput" placeholder="Search">
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="submit" name="srchButton">
                                <i class="bi bi-search"></i>
                            </button>
                        </div>
                    </form>
                </div>
                <div class="my-auto mx-2">
                    <span class="text-center">{{ $user->fname }} {{ $user->lname }}</span>
                </div>
            </div>
            
            <div class="col-6 col-md-4 d-flex justify-content-sm-end" id="leftNavButtons">
                    @if($user->accType=="admin")
                        <a class="nav-link" href="{{route('admin_post')}}" role="button">
                            Dashboard
                            <i class="bi bi-person-lines-fill"></i>
                        </a>
                    @endif
                @foreach ($posts->getpostTypes() as $postType)
                    <a href="/nav?postType={{ $postType->id }}" class="?postType={{ $postType->id }} btn m-auto" role="button">
                        @if ($postType->id == 1)
                            <i class="bi bi-file-post-fill"></i>
                        @endif
                        @if ($postType->id == 2)
                            <i class="bi bi-megaphone"></i>
                        @endif
                        @if ($postType->id == 3)
                            <i class="bi bi-newspaper"></i>
                        @endif
                    </a>
                @endforeach
                <a href="#" class="btn m-auto">
                    <i class="bi bi-bell"></i>
                </a>
                <a href="{{ route('logout') }}" class="btn m-auto">
                    <i class="bi bi-box-arrow-left"></i>
                </a>
            </div>
            
        </div>
    </div>
 
</div>
