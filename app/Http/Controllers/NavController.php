<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\{DB, Hash, Auth};
use Illuminate\Routing\UrlGenerator;
use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\{
    PostController, 
    UserController,
    ParentCommentsController, 
    ChildCommentsController
};
class NavController extends Controller
{ 
    public function leftNavBar(
        Request $request, 
        PostController $posts, 
        UserController $user, 
        ParentCommentsController $parentComments, 
        ChildCommentsController $childComments
        ) {

        $postType = $request->input('postType');

        if($postType == 1) {
            $postType_return = 'main.post';
        }
        elseif ($postType == 2) {
            $postType_return = 'main.announcement';
        }
        elseif ($postType == 3) {
            $postType_return = 'main.news';
        }

        return view($postType_return, [
            'user' => $user->getUserName(),
            'postType' => $postType,
            'posts' => $posts,
            'parentComments' => $parentComments,
            'childComments' => $childComments
        ]);
    }
    
    public function index(
        PostController $posts, 
        UserController $user, 
        ParentCommentsController $parentComments, 
        ChildCommentsController $childComments
        ) { 

        return view('main.index', [
            'user' => $user->getUserName(),
            'posts' => $posts,
            'parentComments' => $parentComments,
            'childComments' => $childComments
        ]);
    }
}
