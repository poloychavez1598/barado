<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StorePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::check()) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'text_content' => 'required_without_all:text_content,file_content',
            'file_content' => 'mimes:doc,docx,pdf,txt,odt,ppt,xlsx,xls,png,jpg,jpeg,PNG,JPG,JPEG|max:10240',
            'link' => 'url:link',
        ];
    }

    public function messages()
    {
        return [
            'text_content.required_without_all' => 'At least text or file field should not be empty when posting',
            'file_content.mimes' => 'Upload file only accepts
                documents (doc,docx,pdf,txt,odt,ppt,xlsx,xls) and
                images (png,jpg,jpeg)',
            'file_content.max' => 'The file size must not exceed 10mb'
        ];
    }
}
