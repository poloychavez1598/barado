<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Free Dikit</title>
        
        <script src="{{ asset('js/app.js') }}"></script>
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">   
        <link rel="stylesheet" href="{{ asset('css/leftNavButtons.css') }}" type="text/css">
    </head>

    <body class="bg-light w-100 m-auto">
        <div class="d-flex flex-column">
            @include('header.nav')
            @yield('main')
        </div>

        <script src="{{ asset('js/leftNavButtons.js') }}"></script>
        <script src="{{ asset('js/textareaTab.js') }}"></script>
        <script src="{{ asset('js/showFileUpload.js') }}"></script>
        <script src="{{ asset('js/toggleReference.js') }}"></script>
    </body>
</html>