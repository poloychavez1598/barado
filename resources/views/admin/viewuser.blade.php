<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="//code.jquery.com/jquery-1.12.3.js"></script>
<script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.css">
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.js"></script>
        <script>
        jQuery(document).ready(function($){
            $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
            });
        })
        </script>
        <script>
           $(document).ready(function() {
            $('#myTable').DataTable(
                {
                });
            } );
            </script>
        <style>
            body {
        overflow-x: hidden;
        }

        #sidebar-wrapper {
        min-height: 100vh;
        margin-left: -15rem;
        -webkit-transition: margin .25s ease-out;
        -moz-transition: margin .25s ease-out;
        -o-transition: margin .25s ease-out;
        transition: margin .25s ease-out;
        }

        #sidebar-wrapper .sidebar-heading {
        padding: 0.875rem 1.25rem;
        font-size: 1.2rem;
        }

        #sidebar-wrapper .list-group {
        width: 15rem;
        }

        #page-content-wrapper {
        min-width: 100vw;
        }

        #wrapper.toggled #sidebar-wrapper {
        margin-left: 0;
        }

        @media (min-width: 768px) {
        #sidebar-wrapper {
            margin-left: 0;
        }

        #page-content-wrapper {
            min-width: 0;
            width: 100%;
        }

        #wrapper.toggled #sidebar-wrapper {
            margin-left: -15rem;
        }
        }
        </style>    
    </head>
    <body>
        <div class="d-flex" id="wrapper">

        <!-- Sidebar -->
        <div class="bg-light border-right" id="sidebar-wrapper">
        <div class="sidebar-heading">
            <a href="/main" class="">
                <img src="{{ asset('image/logo_wallpaper/cic-lc-logo.jpg') }}" width="40" height="40">
            </a>CIC-Free Dikit</div>
        <div class="list-group list-group-flush">
            <a href="{{route('admin_post')}}" class="list-group-item list-group-item-action bg-light">Posts</a>
            <a href="{{route('admin_user')}}" class="list-group-item list-group-item-action bg-light">Users</a>
        </div>
        </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper">

        <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
            <button class="btn btn-primary btn-sm" id="menu-toggle">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-list" viewBox="0 0 16 16">
                    <path fill-rule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z"/>
                </svg>
                Menu
            </button>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                <li class="nav-item active">
                    <a class="text-light" href="/main">
                        <button class="btn btn-success btn-sm" id="menu-toggle">Homepage
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-house-door" viewBox="0 0 16 16">
                                <path d="M8.354 1.146a.5.5 0 0 0-.708 0l-6 6A.5.5 0 0 0 1.5 7.5v7a.5.5 0 0 0 .5.5h4.5a.5.5 0 0 0 .5-.5v-4h2v4a.5.5 0 0 0 .5.5H14a.5.5 0 0 0 .5-.5v-7a.5.5 0 0 0-.146-.354L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.354 1.146zM2.5 14V7.707l5.5-5.5 5.5 5.5V14H10v-4a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5v4H2.5z"/>
                            </svg>
                       </button>
                    </a>
				</li>
                &nbsp;
				<li class="nav-item">
                    <a class="text-light" href="{{route('logout')}}">
                        <button class="btn btn-secondary btn-sm" id="menu-toggle">
                            Logout
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-box-arrow-right" viewBox="0 0 16 16">
                                <path fill-rule="evenodd" d="M10 12.5a.5.5 0 0 1-.5.5h-8a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 .5.5v2a.5.5 0 0 0 1 0v-2A1.5 1.5 0 0 0 9.5 2h-8A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h8a1.5 1.5 0 0 0 1.5-1.5v-2a.5.5 0 0 0-1 0v2z"/>
                                <path fill-rule="evenodd" d="M15.854 8.354a.5.5 0 0 0 0-.708l-3-3a.5.5 0 0 0-.708.708L14.293 7.5H5.5a.5.5 0 0 0 0 1h8.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3z"/>
                            </svg>
                        </button>
                    </a>
				</li>
            </ul> 
            </div>
        </nav>
        <div class="container-fluid p-3" > 
            <div class="container-sm shadow mb-4 bg-white rounded-lg" style="max-width: 700px;">
                <div class="py-3 row">
                    <div class="border-0 col">
                        <span class="font-weight-bolder">User ID:</span><br>
                        <span class="font-weight-bolder">Name:</span><br>
                        <span class="font-weight-bolder">E-mail:</span><br>
                        <span class="font-weight-bolder">Account Type:</span><br>
                        <span class="font-weight-bolder">Date Created:</span><br><br>
                        <span class="font-weight-bolder">Actions:</span>
                    </div>
                    <div class="border-0 col">
                        <span class="font-weight-bolder">{{ $user->id }}</span><br>
                        <span class="font-weight-bolder">{{ $user->fname }} {{ $user->lname }}</span><br>
                        <span class="font-weight-bolder">{{ $user->email }}</span><br>
                        <span class="font-weight-bolder">
                            @if( $user->accType=='user')
                            <span class="badge badge-success">{{ucfirst(trans($user->accType))}}
                            @elseif ($user->accType=='banned')
                            <span class="badge badge-danger">{{ucfirst(trans($user->accType))}}
                            @elseif ($user->accType=='admin')
                            <span class="badge badge-primary">{{ucfirst(trans($user->accType))}}
                            @endif
                            </span>
                        </span><br>
                        <span class="font-weight-bolder">{{ $user->created_at->format('M j, Y - h:i:s A') }}</span><br><br>
                        <div class="flex">
                            @if($user->accType=='banned')
							<a class="text-success" href="#" data-toggle="modal" data-target="#unbanmodal_{{$user->id}}">
                                <button class="btn btn-success btn-sm">Unban User
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-check" viewBox="0 0 16 16">
                                        <path d="M10.97 4.97a.75.75 0 0 1 1.07 1.05l-3.99 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.267.267 0 0 1 .02-.022z" />
                                    </svg>
                                </button>
							</a>
                            @elseif($user->accType=='user' || $user->accType=='admin')
							<a class="text-danger" href="#" data-toggle="modal" data-target="#banmodal_{{$user->id}}">
                                <button class="btn btn-danger btn-sm">Ban User
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-x" viewBox="0 0 16 16">
                                        <path d="M6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H1s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C9.516 10.68 8.289 10 6 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"/>
                                        <path fill-rule="evenodd" d="M12.146 5.146a.5.5 0 0 1 .708 0L14 6.293l1.146-1.147a.5.5 0 0 1 .708.708L14.707 7l1.147 1.146a.5.5 0 0 1-.708.708L14 7.707l-1.146 1.147a.5.5 0 0 1-.708-.708L13.293 7l-1.147-1.146a.5.5 0 0 1 0-.708z"/>
                                    </svg>
                                </button>
							</a>
                            @endif	
						</div>
                        <div class="modal" id="banmodal_{{$user->id}}" tabindex="-1" role="dialog">
                            <form action="{{ route('ban_user', $user->id) }}" method="POST">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            @if($user->accType=='banned') 
                                            <h5 class="modal-title">This user is already banned.</h5>
                                            @else
                                            <h5 class="modal-title">Are you sure you want to ban this user?</h5> 
                                            @endif 
                                        </div>
                                        {{ csrf_field() }}
                                        {{ method_field('PATCH') }}
                                        <div class="modal-footer">
                                            @if($user->accType=='banned')
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            @else
                                            <button type="submit" class="btn btn-primary" href="">Yes</button>
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal" id="unbanmodal_{{$user->id}}" tabindex="-1" role="dialog">
							<form action="{{ route('unban_user', $user->id) }}" method="POST">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											@if($user->accType=='user')
											<h5 class="modal-title">This user is active.</h5>
											@else
											<h5 class="modal-title">Are you sure you want to unban this user?</h5>
											@endif
										</div>
										{{ csrf_field() }}
										{{ method_field('PATCH') }}
										<div class="modal-footer">
											@if($user->accType=='user')
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
											@else
											<button type="submit" class="btn btn-primary" href="">Yes</button>
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
										    @endif
										</div>
									</div>
                        		</div>
							</form>
						</div>
                    </div>
                </div>
            </div>
        
        @foreach ($posts as $post)
        <div class="container-sm shadow mb-4 bg-white rounded-lg">
                <div class="py-3 ">
                    <!-- post header(recepient) -->
                    <div class="border-bottom">
                        <span class="font-weight-bolder">
                            @if($post->post_type_id == 1)
								POST
							@elseif($post->post_type_id == 2)
								ANNOUNCEMENT
							@elseif($post->post_type_id == 3)
								NEWS
                            @endif
                        @if(auth()->user()->accType=="admin")

                        <a style="float:right" class="text-danger" href="#" data-toggle="modal" data-target="#deletemodal_{{$post->id}}">
                            <button class="btn btn-danger btn-sm" id="menu-toggle">Delete Post
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash3" viewBox="0 0 16 16">
                                        <path d="M6.5 1h3a.5.5 0 0 1 .5.5v1H6v-1a.5.5 0 0 1 .5-.5ZM11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3A1.5 1.5 0 0 0 5 1.5v1H2.506a.58.58 0 0 0-.01 0H1.5a.5.5 0 0 0 0 1h.538l.853 10.66A2 2 0 0 0 4.885 16h6.23a2 2 0 0 0 1.994-1.84l.853-10.66h.538a.5.5 0 0 0 0-1h-.995a.59.59 0 0 0-.01 0H11Zm1.958 1-.846 10.58a1 1 0 0 1-.997.92h-6.23a1 1 0 0 1-.997-.92L3.042 3.5h9.916Zm-7.487 1a.5.5 0 0 1 .528.47l.5 8.5a.5.5 0 0 1-.998.06L5 5.03a.5.5 0 0 1 .47-.53Zm5.058 0a.5.5 0 0 1 .47.53l-.5 8.5a.5.5 0 1 1-.998-.06l.5-8.5a.5.5 0 0 1 .528-.47ZM8 4.5a.5.5 0 0 1 .5.5v8.5a.5.5 0 0 1-1 0V5a.5.5 0 0 1 .5-.5Z"/>
                                    </svg>
                            </button>
                        </a>

                            <div class="modal" id="deletemodal_{{$post->id}}" tabindex="-1" role="dialog">
                                @if($post->post_status=="Deleted")
							    <form action="{{ route('delete_permanently', $post->id) }}" method="POST">
								    <div class="modal-dialog" role="document">
									    <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Are you sure you want to delete this post permanently?</h5>
                                            </div>
                                            {{ csrf_field() }}
                                            {{ method_field('PATCH') }}
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary" href="">Yes</button>
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
									</div>
								</form>
                                @else
                                <form action="{{ route('delete_post', $post->id) }}" method="POST">
								    <div class="modal-dialog" role="document">
									    <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Are you sure you want to delete this post?</h5>
                                            </div>
                                            {{ csrf_field() }}
                                            {{ method_field('PATCH') }}
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary" href="">Yes</button>
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
									</div>
								</form>
                                @endif
							</div>
                        @endif
                        @if ($post->user->accType == "admin")
                            <p class="px-1 text-secondary" style="font-size: 13px;">
                                {{ $post->user->accType }}
                            </p>
                            
                        @endif
                        </span>
                        @if ($post->qrcode && $post->qrcode_url)
                            <!-- Button trigger modal(ref) -->
                            <a class="float-right" data-toggle="modal" data-target="#refModal">
                                ref
                            </a>
                            <!-- Modal(ref) -->
                            <div class="modal fade" id="refModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="d-flex justify-content-center mb-2">
                                                <span>click the link below to redirect another page.</span>
                                            </div>
                                            <div class="d-flex justify-content-center">
                                                <a href="{{ $post->qrcode_url }}" target="_blank">{{ $post->qrcode_url }}</a>
                                            </div>
                                            <hr>
                                            <div class="d-flex justify-content-center mb-2">
                                                <span>or scan it from your device.</span>
                                            </div>
                                            <div class="d-flex justify-content-center">
                                                <img src="{{ asset($post->qrcode) }}">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        
                    </div>
    
                     <!-- post body(content) -->
                     <div class="border-bottom py-2">
                        <p class="mb-1">
                        @php
                            $str_toNewline = str_replace(array("\\r\\n", "\\r", "\\n"), "<br>", $post->text_content);
                            echo str_replace("\\t", "&emsp;", $str_toNewline); //String escape the "\t"(new tab)
                            $post->text_content ? "<br>" : '';
                            $extension = pathinfo(storage_path($post->file_content), PATHINFO_EXTENSION);
                            $fileName = pathinfo($post->file_content, PATHINFO_FILENAME);
                            $fileBaseName = pathinfo($post->file_content, PATHINFO_BASENAME);
                            $filenameorig = substr($fileBaseName, strlen($post->id)+1);
                        @endphp
                        @if ($post->file_content)
                            @if ($extension == "doc" || $extension == "docx" || $extension == "pdf" || $extension == "txt" || $extension == "odt" || $extension == "ppt" || $extension == "xlsx" || $extension == "xls")
                                @if ($post->text_content)
                                    <br>
                                @endif
                                <a href="http://127.0.0.1:8000/{{$post->file_content}}" download="{{ $filenameorig }}">
                                    {{ $filenameorig }}
                                </a>
                            @endif
                        @endif
                        </p>
                        @if ($post->file_content)
                            @if ($extension == "png" || $extension == "jpg" || $extension == "jpeg" || $extension == "PNG" || $extension == "JPG" || $extension == "JPEG")
                                <div class="text-center">    
                                    <img src="{{ asset($post->file_content) }}" class="img-fluid" alt="...">
                                </div>
                            @endif
                        @endif
                    </div>
                </div>
            </div> 
        @endforeach
        </div>
        </div>
    </body>
</html>