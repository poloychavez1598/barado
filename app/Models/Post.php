<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    protected $table = 'posts';

    protected $dates = ['time_posted'];

    protected $fillable = [
        'id',
        'post_type_id',
        'user_id',
        'text_content',
        'file_content',
        'qrcode',
        'qrcode_url',
        'post_status',
        'time_posted',
        'time_deleted',
        'deleted_by'
    ];

    public $incrementing = false;

    public $timestamps = false;

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function postType() {
        return $this->belongsTo(PostType::class);
    }

    public function parentComments() {
        return $this->hasMany(ParentComments::class);
    }

    public function childComments() {
        return $this->hasManyThrough(ChildComments::class, ParentComments::class, 
            'posts_id',
            'parent_comments_id'
        );
    }
}
