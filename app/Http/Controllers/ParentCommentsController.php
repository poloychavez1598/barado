<?php

namespace App\Http\Controllers;

use App\Models\ParentComments;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class ParentCommentsController extends Controller
{
    public function getParentComments($post_id) {
        $parentComments = ParentComments::where('posts_id', $post_id)->get();
        
        return $parentComments;
    }

    public function countParentComments($post_id) {
        $parentComments = ParentComments::where('posts_id', $post_id)->count();

        return $parentComments;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\parent_comments  $parent_comments
     * @return \Illuminate\Http\Response
     */
    public function show(parent_comments $parent_comments)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\parent_comments  $parent_comments
     * @return \Illuminate\Http\Response
     */
    public function edit(parent_comments $parent_comments)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\parent_comments  $parent_comments
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, parent_comments $parent_comments)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\parent_comments  $parent_comments
     * @return \Illuminate\Http\Response
     */
    public function destroy(parent_comments $parent_comments)
    {
        //
    }
}
