<?php

namespace App\Http\Controllers;

use App\Models\{Post, PostType};
use App\Http\Controllers\{DateTimeController, UserController};
use App\Http\Requests\StorePostRequest;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\{DB, Auth, Storage};
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Illuminate\Support\Str;
use Carbon\Carbon;
use File;
use Response;
class PostController extends Controller
{
    private $date;
    private $user_id;

    public function __construct() {
        $this->date = new DateTimeController;
        $this->user_id = Auth::id();
    }

    public function getpostTypes() {
        $postTypes = PostType::get();

        return $postTypes;
    }

    // Display date and time created of post in readable format
    public function getDateTimeReadable($time_posted) {
        $dateNow = $this->date->dateNow;

        $setDate = $time_posted->toDateString();

        if ($dateNow->diffInHours($time_posted) < 24) {
            $strDateTime = $time_posted->diffForHumans();
        }
        elseif ($dateNow->diffInHours($time_posted) < 48) {
            if ($dateNow->subHour(24)->toDateString() == $setDate) {
                $strDateTime = 'Yesterday '.date('h:i A', strtotime($time_posted));
            }
            else {
                $strDateTime = $time_posted->toDayDateTimeString();
            }
        }
        else {
            $strDateTime = $time_posted->toDayDateTimeString();
        }

        return $strDateTime;
    }

    public function fileType($id) {
        $post = Post::select('file_content')
            ->where('id', $id)
            ->first();
       
        $filePath = $post->file_content;

        $fileName = pathinfo($filePath, PATHINFO_FILENAME);
        $fileBaseName = pathinfo($filePath, PATHINFO_BASENAME);
        $fileExt = pathinfo($filePath, PATHINFO_EXTENSION);
        
        return [
            'baseFile' => substr($fileBaseName, strlen($id)+1),
            'ext' => $fileExt
        ];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::where('post_status','Approved')->orderBy('time_posted', 'desc')->get();
        
        return $posts;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePostRequest $request)
    {
        $validatePostForm = $request->validated();
        
        // Set the post_id
        $temp = Carbon::now()->format('Y');
        $post_id = $temp."-".Str::random(10);
        
        // Identify escape string and keep it.
        $str_tab = str_replace("\t", "\\t", $request->input('text_content'));
        $text_content = str_replace(array("\r\n", "\r", "\n"), "\\r\\n", $str_tab); // String escape "\r\n"(newline)
        
        // Set new file-name and target path
        $file_path = "";
        $forward = '\\';
        if ($request->file_content) {
            $fileName = $request->file_content->getClientOriginalName();
            $fileExt = $request->file_content->guessExtension();

            if ($fileExt == "png" || $fileExt == "jpg" || $fileExt == "jpeg" || $fileExt == "PNG" || $fileExt == "JPG" || $fileExt == "JPEG") {
                $destinationSubPath = "images".$forward;
            }
            else if ($fileExt == "doc" || $fileExt == "docx" || $fileExt == "pdf" || $fileExt == "txt" || $fileExt == "odt" || $fileExt == "ppt" || $fileExt == "xlsx" || $fileExt == "xls") {
                $destinationSubPath = "documents".$forward;
            }

            $newFileName = $post_id.'_'.$fileName;
            $destination = "post_files".$forward.$destinationSubPath;

            $file_path = $destination.$newFileName;

            $path = $request->file_content->move(public_path($destination), $newFileName);
        }

        $qrcode_url = $request->link;
        $qrcode = "";

        if ($qrcode_url) {
            $qrcode_fileName = $post_id.".svg";
            $qrcode_filePath = "image/qrcode/";
            $qrcode = $qrcode_filePath.$qrcode_fileName;

            QrCode::format('svg')
            ->size(300)
            ->errorCorrection('H')
            ->generate($qrcode_url, $qrcode);
        }

        $post = Post::create([
            'id' => $post_id,
            'post_type_id' => $request->postType,
            'user_id' => $request->user()->id,
            'text_content' => $text_content,
            'file_content' => $file_path,
            'qrcode' => $qrcode,
            'qrcode_url' => $request->link,
            'post_status' => 'Pending',
            'time_posted' => $this->date->dateNow
        ]);

        return redirect()->back()->with('post_querry', $post);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
    }
}
