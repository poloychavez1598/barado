<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user')->insert([
            'id_no' => '0000-0120',
            'fname' => 'Kenith',
            'lname' => 'Iran',
            'password' => Hash::make('Pass@123'),
            'email' => Str::random(10).'@gmail.com',
            'accType' => 'user',
            'datetime_last_session' => NOW(),
            'status' => 'active',
        ]);
    }
}
