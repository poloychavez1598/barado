function toggleRef() {
    var noRef = document.getElementById("noRef");
    var setRef = document.getElementById('setRef');
    var link = document.getElementById('link');

    if (noRef.style.display === "none") {
        noRef.style.display = "block";
        setRef.style.display = "none";
        link.disabled = true;
    }
    else {
        noRef.style.display = "none";
        setRef.style.display = "block";
        link.disabled = false;
    }
}