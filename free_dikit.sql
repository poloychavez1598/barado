-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 05, 2022 at 03:00 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `free_dikit`
--

-- --------------------------------------------------------

--
-- Table structure for table `child_comments`
--

CREATE TABLE `child_comments` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_comments_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `child_comments`
--

INSERT INTO `child_comments` (`id`, `parent_comments_id`, `user_id`, `comment`, `created_at`, `updated_at`) VALUES
('1_0000-0120_2021-09-26 00:00:00_child_0000-0120_2021-09-29 00:58:53', '1_0000-0120_2021-09-26 00:00:00_parent_0000-0110_2021-09-29 00:02:34', '0000-0120', 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Aperiam officia, mollitia aliquam cumque enim doloremque ducimus nostrum culpa obcaecati earum, facilis nemo vel tempora cupiditate eaque, atque expedita quod beatae?', '2021-09-28 16:58:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `queue`, `payload`, `attempts`, `reserved_at`, `available_at`, `created_at`) VALUES
(1, 'default', '{\"uuid\":\"c85594f7-fa90-4efb-b13d-147f09e0f767\",\"displayName\":\"App\\\\Events\\\\Comment\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Broadcasting\\\\BroadcastEvent\",\"command\":\"O:38:\\\"Illuminate\\\\Broadcasting\\\\BroadcastEvent\\\":12:{s:5:\\\"event\\\";O:18:\\\"App\\\\Events\\\\Comment\\\":2:{s:7:\\\"message\\\";s:36:\\\"Comment section is now broadcasting!\\\";s:6:\\\"socket\\\";N;}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1641324650, 1641324650),
(2, 'default', '{\"uuid\":\"e671d345-c324-4ec9-a4f0-f510c8726e57\",\"displayName\":\"App\\\\Events\\\\Comment\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Broadcasting\\\\BroadcastEvent\",\"command\":\"O:38:\\\"Illuminate\\\\Broadcasting\\\\BroadcastEvent\\\":12:{s:5:\\\"event\\\";O:18:\\\"App\\\\Events\\\\Comment\\\":2:{s:7:\\\"message\\\";s:36:\\\"Comment section is now broadcasting!\\\";s:6:\\\"socket\\\";N;}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1641324762, 1641324762),
(3, 'default', '{\"uuid\":\"567841c6-f61e-4809-a940-0b7352c9c1a3\",\"displayName\":\"App\\\\Events\\\\Comment\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Broadcasting\\\\BroadcastEvent\",\"command\":\"O:38:\\\"Illuminate\\\\Broadcasting\\\\BroadcastEvent\\\":12:{s:5:\\\"event\\\";O:18:\\\"App\\\\Events\\\\Comment\\\":2:{s:7:\\\"message\\\";s:36:\\\"Comment section is now broadcasting!\\\";s:6:\\\"socket\\\";N;}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1641324867, 1641324867),
(4, 'default', '{\"uuid\":\"a1fe39fe-a4cd-4cd5-a197-851dfd922641\",\"displayName\":\"App\\\\Events\\\\Comment\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Broadcasting\\\\BroadcastEvent\",\"command\":\"O:38:\\\"Illuminate\\\\Broadcasting\\\\BroadcastEvent\\\":12:{s:5:\\\"event\\\";O:18:\\\"App\\\\Events\\\\Comment\\\":2:{s:7:\\\"message\\\";s:36:\\\"Comment section is now broadcasting!\\\";s:6:\\\"socket\\\";N;}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1641324894, 1641324894),
(5, 'default', '{\"uuid\":\"5b0ddbbe-a35e-4930-9eb9-2c4b0d0b1e4b\",\"displayName\":\"App\\\\Events\\\\Comment\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Broadcasting\\\\BroadcastEvent\",\"command\":\"O:38:\\\"Illuminate\\\\Broadcasting\\\\BroadcastEvent\\\":12:{s:5:\\\"event\\\";O:18:\\\"App\\\\Events\\\\Comment\\\":2:{s:7:\\\"message\\\";s:36:\\\"Comment section is now broadcasting!\\\";s:6:\\\"socket\\\";N;}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1641324934, 1641324934),
(6, 'default', '{\"uuid\":\"f0033421-7c09-4ee7-b486-68b1d44cafbc\",\"displayName\":\"App\\\\Events\\\\Comment\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Broadcasting\\\\BroadcastEvent\",\"command\":\"O:38:\\\"Illuminate\\\\Broadcasting\\\\BroadcastEvent\\\":12:{s:5:\\\"event\\\";O:18:\\\"App\\\\Events\\\\Comment\\\":2:{s:7:\\\"message\\\";s:36:\\\"Comment section is now broadcasting!\\\";s:6:\\\"socket\\\";N;}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1641324974, 1641324974),
(7, 'default', '{\"uuid\":\"861a4add-e285-4b79-a850-f0d5c21a3922\",\"displayName\":\"App\\\\Events\\\\Comment\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Broadcasting\\\\BroadcastEvent\",\"command\":\"O:38:\\\"Illuminate\\\\Broadcasting\\\\BroadcastEvent\\\":12:{s:5:\\\"event\\\";O:18:\\\"App\\\\Events\\\\Comment\\\":2:{s:7:\\\"message\\\";s:36:\\\"Comment section is now broadcasting!\\\";s:6:\\\"socket\\\";N;}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1641324975, 1641324975),
(8, 'default', '{\"uuid\":\"8feb51df-38c9-43c8-9d11-c1805bdd8bb2\",\"displayName\":\"App\\\\Events\\\\Comment\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Broadcasting\\\\BroadcastEvent\",\"command\":\"O:38:\\\"Illuminate\\\\Broadcasting\\\\BroadcastEvent\\\":12:{s:5:\\\"event\\\";O:18:\\\"App\\\\Events\\\\Comment\\\":2:{s:7:\\\"message\\\";s:36:\\\"Comment section is now broadcasting!\\\";s:6:\\\"socket\\\";N;}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1641324976, 1641324976),
(9, 'default', '{\"uuid\":\"685e5634-e13d-4227-8d42-9ecb3c79f478\",\"displayName\":\"App\\\\Events\\\\Comment\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Broadcasting\\\\BroadcastEvent\",\"command\":\"O:38:\\\"Illuminate\\\\Broadcasting\\\\BroadcastEvent\\\":12:{s:5:\\\"event\\\";O:18:\\\"App\\\\Events\\\\Comment\\\":2:{s:7:\\\"message\\\";s:36:\\\"Comment section is now broadcasting!\\\";s:6:\\\"socket\\\";N;}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1641325254, 1641325254),
(10, 'default', '{\"uuid\":\"69f80831-12d4-4193-9da2-9a9dd22726d6\",\"displayName\":\"App\\\\Events\\\\Comment\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Broadcasting\\\\BroadcastEvent\",\"command\":\"O:38:\\\"Illuminate\\\\Broadcasting\\\\BroadcastEvent\\\":12:{s:5:\\\"event\\\";O:18:\\\"App\\\\Events\\\\Comment\\\":2:{s:7:\\\"message\\\";s:36:\\\"Comment section is now broadcasting!\\\";s:6:\\\"socket\\\";N;}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1641325326, 1641325326),
(11, 'default', '{\"uuid\":\"01d4e5e0-e246-439a-8e1b-439469bbf258\",\"displayName\":\"App\\\\Events\\\\Comment\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Broadcasting\\\\BroadcastEvent\",\"command\":\"O:38:\\\"Illuminate\\\\Broadcasting\\\\BroadcastEvent\\\":12:{s:5:\\\"event\\\";O:18:\\\"App\\\\Events\\\\Comment\\\":2:{s:7:\\\"message\\\";s:36:\\\"Comment section is now broadcasting!\\\";s:6:\\\"socket\\\";N;}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1641325365, 1641325365),
(12, 'default', '{\"uuid\":\"d7e29ad3-bf3d-4441-834e-9e0068501c9f\",\"displayName\":\"App\\\\Events\\\\Comment\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Broadcasting\\\\BroadcastEvent\",\"command\":\"O:38:\\\"Illuminate\\\\Broadcasting\\\\BroadcastEvent\\\":12:{s:5:\\\"event\\\";O:18:\\\"App\\\\Events\\\\Comment\\\":2:{s:7:\\\"message\\\";s:36:\\\"Comment section is now broadcasting!\\\";s:6:\\\"socket\\\";N;}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1641325368, 1641325368),
(13, 'default', '{\"uuid\":\"9bdb4a16-6381-4c69-b498-5b59c2a1bcaa\",\"displayName\":\"App\\\\Events\\\\Comment\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Broadcasting\\\\BroadcastEvent\",\"command\":\"O:38:\\\"Illuminate\\\\Broadcasting\\\\BroadcastEvent\\\":12:{s:5:\\\"event\\\";O:18:\\\"App\\\\Events\\\\Comment\\\":2:{s:7:\\\"message\\\";s:36:\\\"Comment section is now broadcasting!\\\";s:6:\\\"socket\\\";N;}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1641325368, 1641325368),
(14, 'default', '{\"uuid\":\"6bc955b2-c620-4d37-a368-a99826463c93\",\"displayName\":\"App\\\\Events\\\\Comment\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Broadcasting\\\\BroadcastEvent\",\"command\":\"O:38:\\\"Illuminate\\\\Broadcasting\\\\BroadcastEvent\\\":12:{s:5:\\\"event\\\";O:18:\\\"App\\\\Events\\\\Comment\\\":2:{s:7:\\\"message\\\";s:36:\\\"Comment section is now broadcasting!\\\";s:6:\\\"socket\\\";N;}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1641325369, 1641325369),
(15, 'default', '{\"uuid\":\"258faa78-e62b-4a18-a412-15a00db32366\",\"displayName\":\"App\\\\Events\\\\Comment\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Broadcasting\\\\BroadcastEvent\",\"command\":\"O:38:\\\"Illuminate\\\\Broadcasting\\\\BroadcastEvent\\\":12:{s:5:\\\"event\\\";O:18:\\\"App\\\\Events\\\\Comment\\\":2:{s:7:\\\"message\\\";s:36:\\\"Comment section is now broadcasting!\\\";s:6:\\\"socket\\\";N;}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1641325442, 1641325442),
(16, 'default', '{\"uuid\":\"ceaf2eca-b6c0-4365-a4d0-00ace18fbb27\",\"displayName\":\"App\\\\Events\\\\Comment\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Broadcasting\\\\BroadcastEvent\",\"command\":\"O:38:\\\"Illuminate\\\\Broadcasting\\\\BroadcastEvent\\\":12:{s:5:\\\"event\\\";O:18:\\\"App\\\\Events\\\\Comment\\\":2:{s:7:\\\"message\\\";s:36:\\\"Comment section is now broadcasting!\\\";s:6:\\\"socket\\\";N;}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1641325463, 1641325463);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2021_09_03_160935_create_posts_table', 1),
(2, '2021_09_28_151918_create_parent_comments_table', 2),
(3, '2021_09_28_152001_create_child_comments_table', 3),
(4, '2021_10_19_191531_users', 4),
(5, '2021_10_19_195757_change_datetime_last_session_data_type', 5),
(6, '2021_12_26_125752_add_qrcode_to_posts_table', 6),
(7, '2021_12_26_133410_add_qrcode_url_to_posts_table', 7),
(8, '2021_12_27_155727_create_jobs_table', 8),
(9, '0000_00_00_000000_create_websockets_statistics_entries_table', 9),
(10, '2022_01_04_205033_change_qrcode_url_data_type', 10);

-- --------------------------------------------------------

--
-- Table structure for table `parent_comments`
--

CREATE TABLE `parent_comments` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `posts_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `parent_comments`
--

INSERT INTO `parent_comments` (`id`, `posts_id`, `user_id`, `comment`, `created_at`, `updated_at`) VALUES
('1_0000-0120_2021-09-26 00:00:00_parent_0000-0110_2021-09-29 00:02:34', '1_0000-0120_2021-09-26 00:00:00', '0000-0110', 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Aperiam officia, mollitia aliquam cumque enim doloremque ducimus nostrum culpa obcaecati earum, facilis nemo vel tempora cupiditate eaque, atque expedita quod beatae?', '2021-09-25 16:02:34', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_type_id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_content` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qrcode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qrcode_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time_posted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `post_type_id`, `user_id`, `text_content`, `file_content`, `qrcode`, `qrcode_url`, `time_posted`) VALUES
('1_0000-0120_2021-09-26 00:00:00', 1, '0000-0120', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut, rerum. At similique explicabo, cum quidem corporis neque hic eos asperiores quia, molestias vero vel dolor, dolores quos sint sequi natus.', 'post_files/images/wallpaper-hd.jpg', '', '', '2021-09-26 00:00:00'),
('1_2021-10-21&02:36:22_yangwenli@gmail.com_2021-11-07 22:35:51', 1, '2021-10-21&02:36:22_yangwenli@gmail.com', 'example\\t\\texample1\\r\\nexample\\r\\n\\r\\nexample', 'post_files/images/1_2021-10-21&02:36:22_yangwenli@gmail.com_2021-11-07 22:35:51_external-content.duckduckgo.com.png', '', '', '2021-11-07 22:35:51'),
('1_2021-10-21&02:36:22_yangwenli@gmail.com_2021-11-07 22:38:15', 1, '2021-10-21&02:36:22_yangwenli@gmail.com', 'check\\tasdasd\\t123456\\t212456\\r\\nasdffhhhhhh\\t24234\\t5345677', '', '', '', '2021-11-07 22:38:15'),
('1_2021-10-22&00:15:47_reinhard123@gmail.com_2021-11-10 01:01:18', 1, '2021-10-22&00:15:47_reinhard123@gmail.com', '', 'post_files/documents/1_2021-10-22&00:15:47_reinhard123@gmail.com_2021-11-10 01:01:18_Undergrad-PRF-DPP-Wordversion.docx', '', '', '2021-11-10 01:01:18'),
('1_2021-10-22&00:15:47_reinhard123@gmail.com_2021-11-13 19:33:47', 1, '2021-10-22&00:15:47_reinhard123@gmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam felis arcu, faucibus in dapibus a, molestie sit amet ipsum. Quisque feugiat vulputate elit. Proin tortor libero, blandit vel nulla sed, congue vulputate lacus. Sed lobortis dictum congue. In bibendum suscipit malesuada. Pellentesque pharetra ultricies quam at facilisis. In hac habitasse platea dictumst. Aliquam quis arcu suscipit, pharetra nibh eu, feugiat sem. Fusce tempus rutrum hendrerit. Aliquam erat volutpat.\\r\\n\\r\\nMaecenas ut sem in nunc aliquam tempus. Quisque ut bibendum ante. Vivamus mattis dictum justo, ut dapibus lorem mattis et. Nam tristique elit eu mattis ultricies. Curabitur in purus massa. Nullam eget velit ut dolor mattis euismod. Etiam aliquet turpis mi, eu tristique lectus rutrum pretium. Vestibulum maximus ante at felis viverra, ac pharetra enim accumsan. Pellentesque feugiat nibh vel orci convallis pretium. Ut aliquet finibus mattis.\\r\\n\\r\\nIn eu scelerisque sapien. Ut volutpat massa et arcu blandit, a sollicitudin nisi ullamcorper. Etiam porttitor pulvinar tortor, nec consectetur odio scelerisque interdum. Quisque sed neque lacus. Nulla facilisi. Sed eget ligula pharetra, facilisis libero maximus, cursus mauris. Morbi odio sem, consectetur non odio id, aliquet tincidunt augue.', 'post_files/documents/1_2021-10-22&00:15:47_reinhard123@gmail.com_2021-11-13 19:33:47_Reading4_andreassen2015.pdf', '', '', '2021-11-13 19:33:47'),
('2_2021-10-20&03:36:01_sasuke@gmail.com_2021-11-13 19:51:29', 2, '2021-10-20&03:36:01_sasuke@gmail.com', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut, rerum. At similique explicabo, cum quidem corporis neque hic eos asperiores quia, molestias vero vel dolor, dolores quos sint sequi natus.', 'post_files/images/2_2021-10-20&03:36:01_sasuke@gmail.com_2021-11-13 19:51:29_walk_5.png', '', '', '2021-11-13 19:51:29'),
('3_0000-0120_2021-09-27 02:50:53', 3, '0000-0120', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut, rerum. At similique explicabo, cum quidem corporis neque hic eos asperiores quia, molestias vero vel dolor, dolores quos sint sequi natus.', '', '', '', '2021-09-27 02:50:53'),
('3_2021-10-20&03:36:01_sasuke@gmail.com_2021-11-13 19:47:49', 3, '2021-10-20&03:36:01_sasuke@gmail.com', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut, rerum. At similique explicabo, cum quidem corporis neque hic eos asperiores quia, molestias vero vel dolor, dolores quos sint sequi natus.\\r\\n\\r\\nLorem ipsum dolor sit amet consectetur adipisicing elit. Ut, rerum. At similique explicabo, cum quidem corporis neque hic eos asperiores quia, molestias vero vel dolor, dolores quos sint sequi natus.\\r\\n\\r\\nLorem ipsum dolor sit amet consectetur adipisicing elit. Ut, rerum. At similique explicabo, cum quidem corporis neque hic eos asperiores quia, molestias vero vel dolor, dolores quos sint sequi natus.', 'post_files/documents/3_2021-10-20&03:36:01_sasuke@gmail.com_2021-11-13 19:47:49_Reading1_ee_2016_03_Mashingaidze.pdf', '', '', '2021-11-13 19:47:49'),
('3_2021-12-05&09:59:14_adolf1921@gmail.com_2022-01-05 21:55:24', 3, '2021-12-05&09:59:14_adolf1921@gmail.com', '[𝗨𝗦𝗲𝗣 𝗙𝗥𝗘𝗘𝗗𝗢𝗠 𝗢𝗙 𝗜𝗡𝗙𝗢𝗥𝗠𝗔𝗧𝗜𝗢𝗡 𝗨𝗣𝗗𝗔𝗧𝗘] Underscoring the directives of Executive Order No. 02 signed by President Duterte, which provides a policy for full public disclosure of all government records in the Executive Branch involving public interest and which upholds the people’s constitutional right to information on matters of public concern, the University of Southeastern Philippines has adopted the operationalization of the Freedom of Information (FOI) program.\\r\\n\\r\\nSince the inception of the program in 2017, USeP President Dr. Lourdes C. Generalao has continued to uphold the University’s commitment towards good governance, accountability, and transparency through FOI.\\r\\n\\r\\nAs of 03 January 2022, the University has received and processed a total of 515 FOI requests, of which 490 of these were successfully provided; 24 were denied since those were either under the FOI exceptions list or not being handled, maintained, or stored by the University, or were considered invalid. One was closed since the requesting party failed to provide the information needed for clarification 60 calendar days after the \"Awaiting Clarification\" status.', '', 'image/qrcode/3_2021-12-05&09:59:14_adolf1921@gmail.com_2022-01-05 21:55:24.svg', 'https://www.facebook.com/USePofficial/photos/a.4504261412958553/4951717231546300/', '2022-01-05 21:55:24'),
('3_2021-12-05&09:59:14_adolf1921@gmail.com_2022-01-05 21:56:38', 3, '2021-12-05&09:59:14_adolf1921@gmail.com', '[𝗨𝗦𝗲𝗣 𝗙𝗥𝗘𝗘𝗗𝗢𝗠 𝗢𝗙 𝗜𝗡𝗙𝗢𝗥𝗠𝗔𝗧𝗜𝗢𝗡 𝗨𝗣𝗗𝗔𝗧𝗘] Underscoring the directives of Executive Order No. 02 signed by President Duterte, which provides a policy for full public disclosure of all government records in the Executive Branch involving public interest and which upholds the people’s constitutional right to information on matters of public concern, the University of Southeastern Philippines has adopted the operationalization of the Freedom of Information (FOI) program.\\r\\n\\r\\nSince the inception of the program in 2017, USeP President Dr. Lourdes C. Generalao has continued to uphold the University’s commitment towards good governance, accountability, and transparency through FOI.\\r\\n\\r\\nAs of 03 January 2022, the University has received and processed a total of 515 FOI requests, of which 490 of these were successfully provided; 24 were denied since those were either under the FOI exceptions list or not being handled, maintained, or stored by the University, or were considered invalid. One was closed since the requesting party failed to provide the information needed for clarification 60 calendar days after the \"Awaiting Clarification\" status.', 'post_files/images/3_2021-12-05&09:59:14_adolf1921@gmail.com_2022-01-05 21:56:38_270233056_4951717224879634_5433336998501999010_n.jpg', '', NULL, '2022-01-05 21:56:38'),
('3_2021-12-05&09:59:14_adolf1921@gmail.com_2022-01-05 21:57:11', 3, '2021-12-05&09:59:14_adolf1921@gmail.com', '[𝗨𝗦𝗲𝗣 𝗙𝗥𝗘𝗘𝗗𝗢𝗠 𝗢𝗙 𝗜𝗡𝗙𝗢𝗥𝗠𝗔𝗧𝗜𝗢𝗡 𝗨𝗣𝗗𝗔𝗧𝗘] Underscoring the directives of Executive Order No. 02 signed by President Duterte, which provides a policy for full public disclosure of all government records in the Executive Branch involving public interest and which upholds the people’s constitutional right to information on matters of public concern, the University of Southeastern Philippines has adopted the operationalization of the Freedom of Information (FOI) program.\\r\\n\\r\\nSince the inception of the program in 2017, USeP President Dr. Lourdes C. Generalao has continued to uphold the University’s commitment towards good governance, accountability, and transparency through FOI.\\r\\n\\r\\nAs of 03 January 2022, the University has received and processed a total of 515 FOI requests, of which 490 of these were successfully provided; 24 were denied since those were either under the FOI exceptions list or not being handled, maintained, or stored by the University, or were considered invalid. One was closed since the requesting party failed to provide the information needed for clarification 60 calendar days after the \"Awaiting Clarification\" status.', 'post_files/images/3_2021-12-05&09:59:14_adolf1921@gmail.com_2022-01-05 21:57:11_270233056_4951717224879634_5433336998501999010_n.jpg', 'image/qrcode/3_2021-12-05&09:59:14_adolf1921@gmail.com_2022-01-05 21:57:11.svg', 'https://www.facebook.com/USePofficial/photos/a.4504261412958553/4951717231546300/', '2022-01-05 21:57:11');

-- --------------------------------------------------------

--
-- Table structure for table `post_type`
--

CREATE TABLE `post_type` (
  `id` int(10) UNSIGNED NOT NULL,
  `postType_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post_type`
--

INSERT INTO `post_type` (`id`, `postType_name`) VALUES
(1, 'POST'),
(2, 'ANNOUNCEMENT'),
(3, 'NEWS');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `accType` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `datetime_last_session` datetime DEFAULT NULL,
  `isActive` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fname`, `lname`, `password`, `email`, `accType`, `datetime_last_session`, `isActive`, `created_at`, `updated_at`) VALUES
('0000-0000', 'Super User', 'Admin', '$2y$10$dZPHA2HhO9fnwtwA4rvvfOE.CBjS7wYqM5yTnAvQXG3.rsRNHrEp6', 'sample@email.com', 'su_admin', '2021-08-02 23:24:24', 1, NULL, NULL),
('0000-0010', 'Admin1', 'Admin1', '$2y$10$dZPHA2HhO9fnwtwA4rvvfOE.CBjS7wYqM5yTnAvQXG3.rsRNHrEp6', 'wala@gmail.com', 'admin', '2021-07-26 13:59:39', 1, NULL, NULL),
('0000-0100', 'User1', 'User1', '$2y$10$dZPHA2HhO9fnwtwA4rvvfOE.CBjS7wYqM5yTnAvQXG3.rsRNHrEp6', 'wala1@gmail.com', 'user', '2021-06-24 23:46:13', 1, NULL, NULL),
('0000-0110', 'Lin Ken', 'Park', '$2y$10$J.4E2fdfMGIIBCGPgaX3.eM4odrMrZVBaa2P9umgYRYBPHg7wIFhK', '0ynM0gCG5f@gmail.com', 'user', '2021-09-14 15:15:36', 1, NULL, NULL),
('0000-0120', 'Kenith', 'Iran', '$2y$10$QoK9A5jWBNDtrs9Nc9LCZOcfsUzLP89kmBMjqJMYXXsAAwY1/nnei', 'GpoU9wBF7f@gmail.com', 'user', '2021-09-14 15:18:47', 1, NULL, NULL),
('2021-10-20&03:36:01_sasuke@gmail.com', 'Sasuke', 'Uchiha', '$2y$10$Ra7AoQo44Qqi6jf0noQBku3UN5PEUI.94/KeGYMqqyLbQtKcXijqW', 'sasuke@gmail.com', 'user', '2021-10-20 03:36:01', 1, '2021-10-19 19:36:01', '2021-10-19 19:36:01'),
('2021-10-21&02:36:22_yangwenli@gmail.com', 'Yang', 'Wenli', '$2y$10$pAKlr/WsgYcALspsSlnSre8lg/9xW1qzSaJKiIBg9f.zRlAVOg6Ue', 'yangwenli@gmail.com', 'user', NULL, 1, '2021-10-20 18:36:22', '2021-10-20 18:36:22'),
('2021-10-22&00:15:47_reinhard123@gmail.com', 'Reinhard', 'Lohengramm', '$2y$10$RWJoUO3sICM9kZt2Jy2PbOXUYUEEZEAN95reToIX3vcZHxGzISChe', 'reinhard123@gmail.com', 'user', NULL, 1, '2021-10-21 16:15:47', '2021-10-21 16:15:47'),
('2021-12-05&09:59:14_adolf1921@gmail.com', 'Adolf', 'Hitler', '$2y$10$uOPBVHF8IfEdN80djz16yOId3qDzA.JAAyGm1tIANWJiZvQERMpD.', 'adolf1921@gmail.com', 'user', NULL, 1, '2021-12-05 01:59:15', '2021-12-05 01:59:15');

-- --------------------------------------------------------

--
-- Table structure for table `websockets_statistics_entries`
--

CREATE TABLE `websockets_statistics_entries` (
  `id` int(10) UNSIGNED NOT NULL,
  `app_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `peak_connection_count` int(11) NOT NULL,
  `websocket_message_count` int(11) NOT NULL,
  `api_message_count` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `child_comments`
--
ALTER TABLE `child_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `child_comments_parent_comments_id_foreign` (`parent_comments_id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parent_comments`
--
ALTER TABLE `parent_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_comments_posts_id_foreign` (`posts_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_post_type_id_foreign` (`post_type_id`),
  ADD KEY `post_user_id_foreign` (`user_id`) USING BTREE;

--
-- Indexes for table `post_type`
--
ALTER TABLE `post_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `websockets_statistics_entries`
--
ALTER TABLE `websockets_statistics_entries`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `websockets_statistics_entries`
--
ALTER TABLE `websockets_statistics_entries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `child_comments`
--
ALTER TABLE `child_comments`
  ADD CONSTRAINT `child_comments_parent_comments_id_foreign` FOREIGN KEY (`parent_comments_id`) REFERENCES `parent_comments` (`id`);

--
-- Constraints for table `parent_comments`
--
ALTER TABLE `parent_comments`
  ADD CONSTRAINT `parent_comments_posts_id_foreign` FOREIGN KEY (`posts_id`) REFERENCES `posts` (`id`);

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `posts_post_type_id_foreign` FOREIGN KEY (`post_type_id`) REFERENCES `post_type` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
