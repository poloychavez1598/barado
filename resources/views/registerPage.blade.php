<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Free Dikit | Register</title>
        
        <script src="{{ asset('js/app.js') }}"></script>
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">     
    </head>

    <body class="bg-light w-100 m-auto">
        <div class="container-fluid" style="max-width: 420px;">
            @if (!empty($user))
                <div class="modal fade mt-5" id="staticBackdrop_reg_success" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content alert alert-success" role="alert">
                            <div class="modal-header alert-heading">
                                <h5 class="modal-title" id="staticBackdropLabel">Registered Successfully</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                You registered successfully and you can now login to <a href="/" class="alert-link">CIC FreeDikit</a> page.
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            <div class="container-sm shadow-sm bg-white rounded-lg mt-5 px-0">
                <div class="p-3 mb-3 border rounded-top d-flex" style="background-color: #e9ecef;">
                    <b class="w-100 text-center">REGISTER</b>
                    <a class="flex-shrink-1 text-dark" href="/">
                        <i class="bi bi-arrow-return-left"></i>
                    </a>
                </div>
                <form class="px-3" action="/register" method="POST">
                    @csrf
                    <div class="form-group form-row">
                        <div class="col">
                            <input type="text" class="form-control" placeholder="First name" 
                                name="first_name"
                                value="{{ old('first_name') }}"
                            >
                        </div>
                        <div class="col">
                            <input type="text" class="form-control" placeholder="Last name" 
                                name="last_name"
                                value="{{ old('last_name') }}"
                            >
                        </div>
                    </div>
                    <div class="form-group">
                        @error('first_name')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                        @error('last_name')
                            <br>
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" id="exampleInputEmail1" 
                            aria-describedby="emailHelp" 
                            placeholder="Email"
                            name="email"
                            value="{{ old('email') }}"
                        >
                        @error('email')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" id="exampleInputPassword1" 
                            placeholder="Password"
                            name="pass"
                            value="{{ old('pass') }}"
                        >
                        @error('pass')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" id="exampleInputPassword1" 
                            placeholder="Re-type Password"
                            name="retype_password"
                            value="{{ old('retype_password') }}"
                        >
                        @error('retype_password')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-success form-control mb-3">Create</button>
                </form>
            </div>
        </div>

        <script type="text/javascript">
            $(window).on('load', function() {
                $('#staticBackdrop_reg_success').modal('show');
            });
        </script>
    </body>
</html>