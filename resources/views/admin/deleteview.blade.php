<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Laravel</title>
	<!-- Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<script src="//code.jquery.com/jquery-1.12.3.js"></script>
	<script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.css">
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.js"></script>
	<script>
	jQuery(document).ready(function($) {
		$("#menu-toggle").click(function(e) {
			e.preventDefault();
			$("#wrapper").toggleClass("toggled");
		});
	})
	</script>
	<script>
	$(document).ready(function() {
		$('#myTable').DataTable({});
	});
	</script>
	<style>
	body {
		overflow-x: hidden;
	}

	#sidebar-wrapper {
		min-height: 100vh;
		margin-left: -15rem;
		-webkit-transition: margin .25s ease-out;
		-moz-transition: margin .25s ease-out;
		-o-transition: margin .25s ease-out;
		transition: margin .25s ease-out;
	}

	#sidebar-wrapper .sidebar-heading {
		padding: 0.875rem 1.25rem;
		font-size: 1.2rem;
	}

	#sidebar-wrapper .list-group {
		width: 15rem;
	}

	#page-content-wrapper {
		min-width: 100vw;
	}

	#wrapper.toggled #sidebar-wrapper {
		margin-left: 0;
	}

	@media (min-width: 768px) {
		#sidebar-wrapper {
			margin-left: 0;
		}

		#page-content-wrapper {
			min-width: 0;
			width: 100%;
		}

		#wrapper.toggled #sidebar-wrapper {
			margin-left: -15rem;
		}
	}
	</style>
</head>

<body>
	<div class="d-flex" id="wrapper">
		<!-- Sidebar -->
		<div class="bg-light border-right" id="sidebar-wrapper">
			<div class="sidebar-heading">
            <a href="/main" class="">
                <img src="{{ asset('image/logo_wallpaper/cic-lc-logo.jpg') }}" width="40" height="40">
            </a>CIC-Free Dikit</div>
			<div class="list-group list-group-flush">
				<a href="{{route('admin_post')}}" class="list-group-item list-group-item-action bg-light">Posts</a>
				<a href="{{route('admin_user')}}" class="list-group-item list-group-item-action bg-light">Users</a>
			</div>
		</div>
		<!-- /#sidebar-wrapper -->
		<!-- Page Content -->
		<div id="page-content-wrapper">
			<nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
            <button class="btn btn-primary btn-sm" id="menu-toggle">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-list" viewBox="0 0 16 16">
                    <path fill-rule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z"/>
                </svg>
                Menu
            </button>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                        <li class="nav-item">
                            <a class="text-light" href="#" data-toggle="modal" data-target="#deleteallmodal">
                                <button class="btn btn-danger btn-sm" id="menu-toggle">Delete all data
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash3" viewBox="0 0 16 16">
                                        <path d="M6.5 1h3a.5.5 0 0 1 .5.5v1H6v-1a.5.5 0 0 1 .5-.5ZM11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3A1.5 1.5 0 0 0 5 1.5v1H2.506a.58.58 0 0 0-.01 0H1.5a.5.5 0 0 0 0 1h.538l.853 10.66A2 2 0 0 0 4.885 16h6.23a2 2 0 0 0 1.994-1.84l.853-10.66h.538a.5.5 0 0 0 0-1h-.995a.59.59 0 0 0-.01 0H11Zm1.958 1-.846 10.58a1 1 0 0 1-.997.92h-6.23a1 1 0 0 1-.997-.92L3.042 3.5h9.916Zm-7.487 1a.5.5 0 0 1 .528.47l.5 8.5a.5.5 0 0 1-.998.06L5 5.03a.5.5 0 0 1 .47-.53Zm5.058 0a.5.5 0 0 1 .47.53l-.5 8.5a.5.5 0 1 1-.998-.06l.5-8.5a.5.5 0 0 1 .528-.47ZM8 4.5a.5.5 0 0 1 .5.5v8.5a.5.5 0 0 1-1 0V5a.5.5 0 0 1 .5-.5Z" />
                                    </svg>
                                </button>
                            </a>
                        </li>
                        &nbsp;
						<li class="nav-item active">
                            <a class="text-light" href="/main">
                                <button class="btn btn-success btn-sm" id="menu-toggle">Homepage
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-house-door" viewBox="0 0 16 16">
                                        <path d="M8.354 1.146a.5.5 0 0 0-.708 0l-6 6A.5.5 0 0 0 1.5 7.5v7a.5.5 0 0 0 .5.5h4.5a.5.5 0 0 0 .5-.5v-4h2v4a.5.5 0 0 0 .5.5H14a.5.5 0 0 0 .5-.5v-7a.5.5 0 0 0-.146-.354L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.354 1.146zM2.5 14V7.707l5.5-5.5 5.5 5.5V14H10v-4a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5v4H2.5z"/>
                                    </svg>
                                </button>
                            </a>
						</li>
                        &nbsp;
						<li class="nav-item">
                            <a class="text-light" href="{{route('logout')}}">
                                <button class="btn btn-secondary btn-sm" id="menu-toggle">Logout
									<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-box-arrow-right" viewBox="0 0 16 16">
										<path fill-rule="evenodd" d="M10 12.5a.5.5 0 0 1-.5.5h-8a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 .5.5v2a.5.5 0 0 0 1 0v-2A1.5 1.5 0 0 0 9.5 2h-8A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h8a1.5 1.5 0 0 0 1.5-1.5v-2a.5.5 0 0 0-1 0v2z"/>
										<path fill-rule="evenodd" d="M15.854 8.354a.5.5 0 0 0 0-.708l-3-3a.5.5 0 0 0-.708.708L14.293 7.5H5.5a.5.5 0 0 0 0 1h8.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3z"/>
									</svg>
                                </button>
                            </a>
						</li>
					</ul>
				</div>
			</nav>
			<div class="container-fluid p-3">
				<div class="table-responsive">
					<table class="table" id="myTable">
						<thead>
							<tr>
								<th scope="col">Content</th>
								<th scope="col">Post Type</th>
								<th scope="col">Posted By</th>
								<th scope="col">Date Created</th>
								<th scope="col">Date Deleted</th>
								<th scope="col">Actions</th>
							</tr>
						</thead>
						<tbody> 
                            @foreach($delete as $post) 
								<tr id="{{$post->time_deleted}}">
								<td scope="row">{{\Illuminate\Support\Str::limit($post->text_content, 30)}}</td>
								<td>
									<h6>
									@if($post->post_type_id == 1)
									<span class="badge badge-primary">Post
									@elseif($post->post_type_id == 2)
									<span class="badge badge-dark">Announcement
									@elseif($post->post_type_id == 3)
									<span class="badge badge-warning">News
									@endif
									</span>
									</h6>
                                </td>
								<td>{{$post->user->fname . ' ' . $post->user->lname}}</td>
								<td>{{$post->time_posted->format('M j, Y - h:i:s A')}}</td>
								<td>
                                    @if($post->time_deleted==null)
                                    No date
                                    @else
                                    {{ Carbon\Carbon::parse($post->time_deleted)->format('M j, Y - h:i:s A') }}
                                    @endif
                                </td>
								<td>
									<div class="flex text-center">
										<a class="text-primary mr-1" href="{{ route('view_post', $post->id) }}">
											<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-eye-fill" viewBox="0 0 16 16">
												<path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z" />
												<path d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z" />
											</svg>
										</a>
										<a class="text-success" href="#" data-toggle="modal" data-target="#approvemodal_{{$post->id}}">
											<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-check" viewBox="0 0 16 16">
												<path d="M10.97 4.97a.75.75 0 0 1 1.07 1.05l-3.99 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.267.267 0 0 1 .02-.022z" />
											</svg>
										</a>
										<a class="text-danger" href="#" data-toggle="modal" data-target="#deletemodal_{{$post->id}}">
											<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash3" viewBox="0 0 16 16">
												<path d="M6.5 1h3a.5.5 0 0 1 .5.5v1H6v-1a.5.5 0 0 1 .5-.5ZM11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3A1.5 1.5 0 0 0 5 1.5v1H2.506a.58.58 0 0 0-.01 0H1.5a.5.5 0 0 0 0 1h.538l.853 10.66A2 2 0 0 0 4.885 16h6.23a2 2 0 0 0 1.994-1.84l.853-10.66h.538a.5.5 0 0 0 0-1h-.995a.59.59 0 0 0-.01 0H11Zm1.958 1-.846 10.58a1 1 0 0 1-.997.92h-6.23a1 1 0 0 1-.997-.92L3.042 3.5h9.916Zm-7.487 1a.5.5 0 0 1 .528.47l.5 8.5a.5.5 0 0 1-.998.06L5 5.03a.5.5 0 0 1 .47-.53Zm5.058 0a.5.5 0 0 1 .47.53l-.5 8.5a.5.5 0 1 1-.998-.06l.5-8.5a.5.5 0 0 1 .528-.47ZM8 4.5a.5.5 0 0 1 .5.5v8.5a.5.5 0 0 1-1 0V5a.5.5 0 0 1 .5-.5Z" />
											</svg>
										</a>
									</div>
									<div class="modal" id="deletemodal_{{$post->id}}" tabindex="-1" role="dialog">
											<form action="{{ route('delete_permanently', $post->id) }}" method="POST">
												<div class="modal-dialog" role="document">
													<div class="modal-content">
														<div class="modal-header">
															<h5 class="modal-title">Are you sure you want to delete this post permanently?</h5>
														</div>
														{{ csrf_field() }}
														{{ method_field('PATCH') }}
														<div class="modal-footer">
															<button type="submit" class="btn btn-primary" href="">Yes</button>
															<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
														</div>
													</div>
                                                </div>
											</form>
                                        </div>
                                        <div class="modal" id="deleteallmodal" tabindex="-1" role="dialog">
											<form action="{{route('delete_all')}}" method="POST">
												<div class="modal-dialog" role="document">
													<div class="modal-content">
														<div class="modal-header">
															<h5 class="modal-title">Are you sure you want to delete all deleted posts permanently?</h5>
														</div>
                                                        {{ csrf_field() }}
														{{ method_field('POST') }}
														<div class="modal-footer">
															<button type="submit" class="btn btn-primary" href="">Yes</button>
															<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
														</div>
													</div>
                                                </div>
											</form>
										</div>
										<div class="modal" id="approvemodal_{{$post->id}}" tabindex="-1" role="dialog">
											<form action="{{ route('restore_post', $post->id) }}" method="POST">
												<div class="modal-dialog" role="document">
													<div class="modal-content">
														<div class="modal-header">
                                                            @if($post->post_status=='Approved')
                                                            <h5 class="modal-title">This post is already approved.</h5>
                                                            @else
															<h5 class="modal-title">Are you sure you want to approve this post?</h5>
                                                            @endif
														</div>
														{{ csrf_field() }}
														{{ method_field('PATCH') }}
														<div class="modal-footer">
                                                            @if($post->post_status=='Approved')
															<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                            @else
                                                            <button type="submit" class="btn btn-primary" href="">Yes</button>
															<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                            @endif
														</div>
													</div>
                                                </div>
											</form>
										</div>
									</td>
								</tr> 
							@endforeach 
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</body>

</html>