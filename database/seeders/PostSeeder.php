<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
            'id' => '3_'.'0000-0120_'.NOW(),
            'post_type_id' => '3',
            'user_id' => '0000-0120',
            'text_content' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut, rerum. At similique explicabo, cum quidem corporis neque hic eos asperiores quia, molestias vero vel dolor, dolores quos sint sequi natus.',
            'file_content' => '',
            'time_posted' => NOW(),
        ]);
    }
}
