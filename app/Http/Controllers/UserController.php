<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\{DB, Hash, Auth};
use Illuminate\Validation\Rules\Password;
use App\Http\Controllers\DateTimeController;
use Illuminate\Support\Str;
use Carbon\Carbon;

class UserController extends Controller
{
    private $date;
    private $user_id;

    public function __construct() {
        $this->date = new DateTimeController();
        $this->user_id = Auth::id();
    }

    public function getUserName() {
        $users = DB::table('users')
            ->where('id', $this->user_id)
            ->first();

        return $users;
    }

    public function registrationForm(Request $request) {
        
        $validate_registration = $request->validate([ 
            'first_name' => ['required', 'regex:/^[a-zA-Z\s]*$/', 'max:30'],
            'last_name' => ['required', 'regex:/^[a-zA-Z\s]*$/', 'max:20'],
            'email' => 'email:rfc,dns|unique:users,email',
            'pass' => [
                'required', 
                Password::min(8)
                    ->letters()
                    ->mixedCase()
                    ->numbers()
                    ->symbols()
            ],
            'retype_password' => 'required|same:pass',
        ]);

        $temp = Carbon::now()->format('Y');
        $id = $temp."-".Str::random(12);
        $first_name =  strtolower($request->input('first_name'));
        $last_name =  strtolower($request->input('last_name'));
        
        $user = User::create([
            'id' => $id,
            'fname' => ucwords(trans($first_name)),
            'lname' => ucwords(trans($last_name)),
            'password' => Hash::make($request->input('pass')),
            'email' => $request->input('email'),
            'accType' => 'user',
            'isActive' => '1',
        ]);

        return view('registerPage')->with('user', $user);
    }

    public function loginForm(Request $request) {
        $validate_login_user = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        if (Auth::attempt($validate_login_user)) {       
            if(Auth::user()->accType=="admin"){
                $request->session()->regenerate();
                $id = Auth::user()->id;
                $session = User::where('id',$id)->update(['datetime_last_session'=>now()]);
                return redirect()->route('admin_post');    
            }
            elseif(Auth::user()->accType=="user"){
                $request->session()->regenerate();
                $id = Auth::user()->id;
                $session = User::where('id',$id)->update(['datetime_last_session'=>now()]);
                return redirect()->intended('/main');
            }else{
                return back()->withErrors([
                    'banned' => 'You are banned.'
                ]);
            }
        }
        
        return back()->withErrors([
            'email' => 'The provided credentials do not match our records.',
            'password' => 'The provided password does not match our records.'
        ]);
    }

    public function logout(Request $request) {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect(route('login'));
    }
}
