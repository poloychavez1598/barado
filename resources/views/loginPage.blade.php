<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Free Dikit | Login</title>
    
    <script src="{{ asset('js/app.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">     
  </head>

  <body class="bg-light w-100 m-auto">

    <div class="container-fluid" style="max-width: 420px;">
      @if (!empty($message))
        <div class="modal fade mt-5" id="staticBackdrop_login_redirect" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
          <div class="modal-dialog">
              <div class="modal-content alert alert-danger" role="alert">
                  <div class="modal-header alert-heading">
                      <h5 class="modal-title" id="staticBackdropLabel">{{ $message }}</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
              </div>
          </div>
        </div>
      @endif
      <div class="container-sm shadow-sm bg-white rounded-lg mt-5 px-0">
        <div class="p-3 mb-3 text-center border rounded-top" style="background-color: #e9ecef;">
          <b>LOGIN</b>
        </div>
        <form class="px-3" action="/login" method="POST">
          @csrf
          <div class="form-group">
            <input type="email" name="email" class="form-control"
              id="exampleInputEmail1" 
              aria-describedby="emailHelp" 
              placeholder="Email"
              value="{{ old('email') }}"
            >
            @error('email')
              <span class="text-danger">{{ $message }}</span>
            @enderror
          </div>
          <div class="form-group">
            <input type="password" name="password" class="form-control" 
              id="exampleInputPassword1" 
              placeholder="Password"
              value="{{ old('password') }}"
            >
            @error('password')
              <span class="text-danger">{{ $message }}</span>
            @enderror
            @error('banned')
              <br>
              <div class="alert alert-danger" role="alert">
                <h6><span class="text-danger">{{$message}}<br> Please contact your administrator.</span></h6>
              </div>
            @enderror
          </div>
            <button type="submit" class="btn btn-primary form-control">Login</button>
            <hr>
        </form>
        <div class="pb-3 d-flex justify-content-center">
          <a href="/register" class="text-center">Register Account</a>
        </div>
      </div>
    </div>

    <script type="text/javascript">
      $(window).on('load', function() {
          $('#staticBackdrop_login_redirect').modal('show');
      });
    </script>
  </body>
</html>