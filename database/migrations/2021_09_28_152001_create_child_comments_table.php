<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChildCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('child_comments', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('parent_comments_id');
            $table->string('user_id');
            $table->text('comment');
            $table->timestamps();

            $table->foreign('parent_comments_id')
                ->references('id')
                ->on('parent_comments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('child_comments');
    }
}
