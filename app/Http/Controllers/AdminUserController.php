<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Post;
use Carbon\Carbon;

class AdminUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function admin_user_control()
    {
        $users = User::all();

        return view('admin.user', compact('users'));
    }

    public function user_post($id)
    {
        $get_user = User::findOrFail($id);
        $user = User::where('id', $id)->first();
        $posts = Post::where('user_id', $id)->get();

        return view('admin.viewuser', compact('user','posts'));
    }

    public function reset_password($id){
        $get_user = User::findOrFail($id);
        $user = User::where('id', $id)->first();
        $temp = Carbon::now()->format('Y');
        $password = 'freedikit-'.$temp;

        $reset = User::where('id',$id)->update(['password'=>$password]);
        return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ban_user($id)
    {
        $get_user = User::findOrFail($id);
        $user = User::where('id', $id)->first();

        $ban = "banned";
        $ban = User::where('id', $id)->update(['accType'=>$ban]);
        return redirect()->back();
    }
    public function unban_user($id)
    {
        $get_user = User::findOrFail($id);
        $user = User::where('id', $id)->first();

        $active = "user";
        $ban = User::where('id', $id)->update(['accType'=>$active]);
        return redirect()->back();
    }
}
