// Get the container element
var btnContainer = document.getElementById("leftNavButtons");

// Get buttons with class="<path name of url> btn" inside the container
var btns = btnContainer.getElementsByClassName(window.location.search + " btn");

// Adding className
if (window.location.pathname === "/main"){
    btns[0].className += " ";
}
else {
    btns[0].className += " active rounded-circle";
}
