-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 19, 2021 at 09:40 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `free_dikit`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `accType` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `datetime_last_session` datetime NOT NULL,
  `isActive` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fname`, `lname`, `password`, `email`, `accType`, `datetime_last_session`, `isActive`, `created_at`, `updated_at`) VALUES
('0000-0000', 'Super User', 'Admin', '$2y$10$dZPHA2HhO9fnwtwA4rvvfOE.CBjS7wYqM5yTnAvQXG3.rsRNHrEp6', 'sample@email.com', 'su_admin', '2021-08-02 23:24:24', 1, NULL, NULL),
('0000-0010', 'Admin1', 'Admin1', '$2y$10$dZPHA2HhO9fnwtwA4rvvfOE.CBjS7wYqM5yTnAvQXG3.rsRNHrEp6', 'wala@gmail.com', 'admin', '2021-07-26 13:59:39', 1, NULL, NULL),
('0000-0100', 'User1', 'User1', '$2y$10$dZPHA2HhO9fnwtwA4rvvfOE.CBjS7wYqM5yTnAvQXG3.rsRNHrEp6', 'wala1@gmail.com', 'user', '2021-06-24 23:46:13', 1, NULL, NULL),
('0000-0110', 'Lin Ken', 'Park', '$2y$10$J.4E2fdfMGIIBCGPgaX3.eM4odrMrZVBaa2P9umgYRYBPHg7wIFhK', '0ynM0gCG5f@gmail.com', 'user', '2021-09-14 15:15:36', 1, NULL, NULL),
('0000-0120', 'Kenith', 'Iran', '$2y$10$QoK9A5jWBNDtrs9Nc9LCZOcfsUzLP89kmBMjqJMYXXsAAwY1/nnei', 'GpoU9wBF7f@gmail.com', 'user', '2021-09-14 15:18:47', 1, NULL, NULL),
('2021-10-20&03:36:01_sasuke@gmail.com', 'Sasuke', 'Uchiha', '$2y$10$Ra7AoQo44Qqi6jf0noQBku3UN5PEUI.94/KeGYMqqyLbQtKcXijqW', 'sasuke@gmail.com', 'user', '2021-10-20 03:36:01', 1, '2021-10-19 19:36:01', '2021-10-19 19:36:01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
