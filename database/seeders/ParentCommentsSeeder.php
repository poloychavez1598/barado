<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ParentCommentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        date_default_timezone_set('Asia/Manila');
        $dateNow = \Carbon\Carbon::parse(date('Y-m-d H:i:s'));

        DB::table('parent_comments')->insert([
            'id' => '1_0000-0120_2021-09-26 00:00:00_parent_'.'0000-0110_'.$dateNow,
            'posts_id' => '1_0000-0120_2021-09-26 00:00:00',
            'user_id' => '0000-0110',
            'comment' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Aperiam officia, mollitia aliquam cumque enim doloremque ducimus nostrum culpa obcaecati earum, facilis nemo vel tempora cupiditate eaque, atque expedita quod beatae?',
            'created_at' => $dateNow,
        ]);
    }
}
