@extends('layouts.app')

@section('main')
    <div class="container-fluid" style="max-width: 540px;" id="newsContainer">
        @if (session()->has('post_querry'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                You post successfully.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <div class="container-sm shadow-sm mb-3 bg-secondary text-white rounded-pill w-25">
            <h3 class="d-flex justify-content-center font-weight-bold">NEWS</h3>
        </div>
        <!-- Write Post -->
        <div class="container-sm shadow-sm mb-4 bg-white rounded-lg">
            <form action="/post" class="py-3 m-3" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="postType" value="{{$postType}}" id="">
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">{{ $user->fname }} {{ $user->lname }}</label>
                    <textarea class="form-control" name="text_content" id="my-textarea" rows="3" placeholder="Write your post here..."></textarea>
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                    </div>
                    <div class="custom-file">
                        <input type="file" name="file_content" class="custom-file-input" id="file-upload" aria-describedby="inputGroupFileAddon01">
                        <label class="custom-file-label" id="file-upload-filename" for="inputGroupFile01">Choose file</label>
                    </div>
                </div>

                @foreach ($errors->all() as $message)
                    <span class="text-danger">{{ $message }}</span>
                @endforeach

                <div id="noRef">
                    <button type="button" class="btn btn-primary" onclick="toggleRef()">Ref</button>
                    <button type="submit" class="btn btn-outline-primary float-right">Dikit</button>
                </div>
                <div id="setRef" style="display: none">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <button class="btn btn-primary" type="button" id="button-addon1-ref" onclick="toggleRef()">Ref</button>
                        </div>
                        <input type="text" class="form-control" id="link" name="link" placeholder="Input the URL here. . ." 
                            aria-label="Example text with button addon" 
                            aria-describedby="button-addon1"
                            disabled
                            required
                        >
                    </div>
                    <div class="d-flex justify-content-end">
                        <button type="submit" class="btn btn-outline-primary">Dikit</button>
                    </div>
                </div>
            </form>
        </div>
        
        <!-- NEWS -->
        @foreach ($posts->index() as $post)
            @if ($post->postType->id == $postType)
            <div class="container-sm shadow mb-4 bg-white rounded-lg">

                <div class="py-3 ">
    
                    <!-- post header(recepient) -->
                    <div class="border-bottom">
                        <span class="font-weight-bolder">{{ $post->user->fname }} {{ $post->user->lname }}
                        @if(auth()->user()->accType=="admin")
                        <a href="#" data-toggle="modal" data-target="#deletemodal">
                                delete
                            </a>
                            <div class="modal" id="deletemodal" tabindex="-1" role="dialog">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Are you sure you want to delete this news?</h5>
                                        
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary">Yes</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if ($post->user->accType == "admin")
                            <p class="px-1 text-secondary" style="font-size: 13px;">
                                {{ $post->user->accType }}
                            </p>
                            
                        @endif
                        </span>
                        @if ($post->qrcode && $post->qrcode_url)
                            <!-- Button trigger modal(ref) -->
                            <a class="float-right" data-toggle="modal" data-target="#refModal">
                                ref
                            </a>
                            <!-- Modal(ref) -->
                            <div class="modal fade" id="refModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="d-flex justify-content-center mb-2">
                                                <span>click the link below to redirect another page.</span>
                                            </div>
                                            <div class="d-flex justify-content-center">
                                                <a href="{{ $post->qrcode_url }}" target="_blank">{{ $post->qrcode_url }}</a>
                                            </div>
                                            <hr>
                                            <div class="d-flex justify-content-center mb-2">
                                                <span>or scan it from your device.</span>
                                            </div>
                                            <div class="d-flex justify-content-center">
                                                <img src="{{ asset($post->qrcode) }}">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <span class="text-secondary" style="font-size: 13px;">
                            <p class="mb-1">{{ $posts->getDateTimeReadable($post->time_posted) }}</p>
                        </span>
                    </div>
    
                    <!-- post body(content) -->
                    <div class="border-bottom py-2">
                        <p class="mb-1">
                        @php
                            $str_toNewline = str_replace(array("\\r\\n", "\\r", "\\n"), "<br>", $post->text_content);
                            echo str_replace("\\t", "&emsp;", $str_toNewline); //String escape the "\t"(new tab)
                            $post->text_content ? "<br>" : '';
                        @endphp
                        @if ($post->file_content)
                            @if ($posts->fileType($post->id)['ext'] == "doc" || $posts->fileType($post->id)['ext'] == "docx" || $posts->fileType($post->id)['ext'] == "pdf" || $posts->fileType($post->id)['ext'] == "txt" || $posts->fileType($post->id)['ext'] == "odt" || $posts->fileType($post->id)['ext'] == "ppt" || $posts->fileType($post->id)['ext'] == "xlsx" || $posts->fileType($post->id)['ext'] == "xls")
                                @if ($post->text_content)
                                    <br>
                                @endif
                                <a href="{{ $post->file_content }}" download="{{ $posts->fileType($post->id)['baseFile'] }}">
                                    {{ $posts->fileType($post->id)['baseFile'] }}
                                </a>
                            @endif
                        @endif
                        </p>
                        @if ($post->file_content)
                            @if ($posts->fileType($post->id)['ext'] == "png" || $posts->fileType($post->id)['ext'] == "jpg" || $posts->fileType($post->id)['ext'] == "jpeg" || $posts->fileType($post->id)['ext'] == "PNG" || $posts->fileType($post->id)['ext'] == "JPG" || $posts->fileType($post->id)['ext'] == "JPEG")
                                <img src="{{ asset($post->file_content) }}" class="img-fluid" alt="...">
                            @endif
                        @endif
                    </div>
    
                    <!-- post footer(comment section) -->
                    @php
                        $countChildComments = 0;
                        // count child comments in parent comments
                        foreach ($parentComments->getParentComments($post->id) as $parent) {
                            $countChildComments = $childComments->countChildComments($parent->id);
                        }

                        $total_comments_count = $parentComments->countParentComments($post->id) + $countChildComments;
                    @endphp

                    <div class="my-1 d-flex justify-content-between">
                        <a class="btn btn-light rounded-pill border-0" href="#{{ $post->id }}" role="button">Comment</a> 
                        <span class="text-secondary my-auto" style="font-size: 13px;">
                        @if ($total_comments_count > 0)
                            @if ($total_comments_count == 1)
                                1 Comment
                            @else
                                {{ $total_comments_count }} Comments
                            @endif
                        @else
                            No Comments
                        @endif
                        </span>
                    </div>
                    
                    @foreach ($parentComments->getParentComments($post->id) as $parent)
                        <div class="ml-0 mb-2"> <!-- reply to comment (1st nest)-->
                            <div class="p-2 bg-light rounded-lg mb-1">
                                <span class="font-weight-bolder" style="font-size: 13px;">
                                {{ $parent->user->fname }} {{ $parent->user->lname }}
                                </span>
                                <span class="text-secondary" style="font-size: 12px;"> . 
                                    {{ $posts->getDateTimeReadable($parent->created_at) }}
                                </span>
                                <p class="mb-0">
                            @php
                                $str_toNewline = str_replace(array("\\r\\n", "\\r", "\\n"), "<br>", $parent->comment);
                                echo str_replace("\\t", "&emsp;", $str_toNewline); //String escape the "\t"(new tab)
                            @endphp
                                </p>
                                <a class="text-dark font-weight-bolder" href="#{{ $parent->id }}" role="button" style="font-size: 12px;">Reply</a>
                            </div>
                            <form action="" class="input-group border rounded-lg mb-2" style="max-width: 75%">
                                <input type="text" class="form-control bg-light border-0 rounder-left-lg" placeholder="Write your reply here" id="{{ $parent->id }}">
                                <div class="input-group-append">
                                    <button class="btn btn-outline-secondary bg-light border-0 rounder-right-lg text-dark" type="submit" id="button-addon2">Send</button>
                                </div>
                            </form>
                            
                            @foreach ($childComments->getChildComments($parent->id) as $child)
                                <div class="ml-2">
                                    <div class="ml-5"> <!-- reply to comment (2nd nest)-->
                                        <div class="p-2 bg-light rounded-lg mb-1">
                                            <span class="font-weight-bolder" style="font-size: 13px;">
                                            {{ $child->user->fname }} {{ $child->user->lname }}
                                            </span>
                                            <span class="text-secondary" style="font-size: 12px;"> .
                                                {{ $posts->getDateTimeReadable($child->created_at) }}
                                            </span>
                                            <p class="mb-0">
                                            @php
                                                $str_toNewline = str_replace(array("\\r\\n", "\\r", "\\n"), "<br>", $child->comment);
                                                echo str_replace("\\t", "&emsp;", $str_toNewline); //String escape the "\t"(new tab)
                                            @endphp
                                            </p>
                                            <a class="text-dark font-weight-bolder" href="#{{ $child->id }}" role="button" style="font-size: 12px;">Reply</a>
                                        </div>
                                        <form action="" class="input-group border rounded-lg" style="max-width: 75%">
                                            <input type="text" class="form-control bg-light border-0 rounder-left-lg" placeholder="Write your reply here" id="{{ $child->id }}">
                                            <div class="input-group-append">
                                                <button class="btn btn-outline-secondary bg-light border-0 rounder-right-lg text-dark" type="submit" id="button-addon2">Send</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endforeach
                    
                    <form action="" class="input-group border rounded-lg">
                        <input type="text" class="form-control bg-light border-0 rounder-left-lg" placeholder="Write your comment here" id="{{ $post->id }}">
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary bg-light border-0 rounder-right-lg text-dark" type="submit" id="button-addon2">Send</button>
                        </div>
                    </form>
    
                </div>
    
            </div>    
            @endif
        @endforeach

    </div>
@endsection