<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParentCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parent_comments', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('posts_id');
            $table->string('user_id');
            $table->text('comment');
            $table->timestamps();

            $table->foreign('posts_id')
                ->references('id')
                ->on('posts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parent_comments');
    }
}
