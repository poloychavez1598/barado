<?php

use Illuminate\Support\Facades\{Route, Auth, Redirect};
use App\Http\Controllers\{NavController, PostController, UserController, AdminPostController, AdminUserController};
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (Auth::check()) {
        if(Auth::user()->accType=="banned"){
            Auth::logout();
            return view('loginPage')->withErrors([
                'banned' => 'You are banned.'
            ]);
        }else{
            return redirect()->intended('/main');
        }
    }
    else {
        return view('loginPage');
    }
})->name('login');
Route::post('/login', [UserController::class, 'loginForm']);
Route::get('/logout', [UserController::class, 'logout'])->name('logout');

Route::get('/register', function () {
    if (Auth::check()) {
        return redirect()->intended('/main');
    }
    else {
        return view('registerPage');
    }
});
Route::post('/register', [UserController::class, 'registrationForm']);

Route::match(['get', 'post'], '/main', [NavController::class, 'index'])->middleware('auth');
Route::get('/nav', [NavController::class, 'leftNavBar'])->middleware('auth');

Route::resource('/post', PostController::class)->middleware('auth');

Route::get('/testPage', function () {
    return QrCode::size(300)->generate('qr');
});

//admin middleware
Route::group(['middleware' => ['admin']], function () {
    //routes for posts management
    Route::get('/admin-post', [AdminPostController::class, 'admin_post'])->name('admin_post');
    Route::get('/admin-post/view-post/{id}', [AdminPostController::class, 'view_post'])->name('view_post');
    Route::get('/deleted-post', [AdminPostController::class, 'deleted_post'])->name('deleted_post');
    Route::PATCH('/admin-post/delete-permanently/{id}', [AdminPostController::class, 'delete_permanently'])->name('delete_permanently');
    Route::post('/admin-post/delete-all', [AdminPostController::class, 'delete_all'])->name('delete_all');
    Route::PATCH('/admin-post/restore-post/{id}', [AdminPostController::class, 'approve_from_delete'])->name('restore_post');
    Route::PATCH('/admin-post/delete-post/{id}', [AdminPostController::class, 'delete_post'])->name('delete_post');
    Route::PATCH('/admin-post/approve-post/{id}', [AdminPostController::class, 'approve_post'])->name('approve_post');

    //routes for user management
    Route::get('/admin-user/user-post/{id}', [AdminUserController::class, 'user_post'])->name('user_post');
    Route::get('/admin-user', [AdminUserController::class, 'admin_user_control'])->name('admin_user');
    Route::PATCH('/admin-user/ban-user/{id}', [AdminUserController::class, 'ban_user'])->name('ban_user');
    Route::PATCH('/admin-user/reset-password-user/{id}', [AdminUserController::class, 'reset_password'])->name('reset_password');
    Route::PATCH('/admin-user/unban-user/{id}', [AdminUserController::class, 'unban_user'])->name('unban_user');
});