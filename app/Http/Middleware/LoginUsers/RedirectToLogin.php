<?php

namespace App\Http\Middleware\LoginUsers;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RedirectToLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::check()) {
            $message = "You must login first before you can access the page.";

            return back()->with('message', $message);
        }
        return $next($request);
    }
}
