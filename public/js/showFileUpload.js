var input = document.getElementById( 'file-upload' );
var infoArea = document.getElementById( 'file-upload-filename' );

input.addEventListener( 'change', showFileName );

function showFileName( event ) {
  
  // the change event gives us the input it occurred in 
  if (!event || !event.target || !event.target.files ||  event.target.files.length === 0) {
    return;
  }
  const name = event.target.files[0].name;
  const lastDot = name.lastIndexOf('.');
  
  // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
  const fileName = name.substring(0, lastDot);
  const ext = name.substring(lastDot + 1);
  
  // use fileName however fits your app best, i.e. add it into a div
  infoArea.textContent = fileName + '.' + ext;
}