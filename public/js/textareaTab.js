document.getElementById('my-textarea').addEventListener('keydown', function(e) {
  if (e.key == 'Tab') {
    var cIndex=this.selectionStart;
    this.value=[this.value.slice(0,cIndex),//Slice at cursor index
        "\t",                              //Add Tab
        this.value.slice(cIndex)].join('');//Join with the end
    e.stopPropagation();
    e.preventDefault();                //Don't quit the area
    this.selectionStart=cIndex+1;
    this.selectionEnd=cIndex+1; //Keep the cursor in the right index
  }
});