<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

date_default_timezone_set('Asia/Manila');

class DateTimeController extends Controller
{
    public $dateNow;

    public function __construct() {
        $this->dateNow = Carbon::parse(date('Y-m-d H:i:s'));
    }

    public function getDateToString() {
        return $this->dateNow->toDateString();
    }

    public function getTimeToString() {
        return $this->dateNow->toTimeString();
    }
}
