<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use Illuminate\Support\Facades\Auth;


class AdminPostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function admin_post()
    {
        $posts = Post::where('post_status','Approved')->OrWhere('post_status','Pending')->orderBy('time_posted','asc')->with('user')->get();
         
        return view('admin.post', compact('posts'));
    }

    public function deleted_post(){
        $delete = Post::where('post_status','Deleted')->with('user')->orderBy('time_deleted','ASC')->get();
         
        return view('admin.deleteview', compact('delete'));
    }

    public function delete_all(){
        $delete = Post::where('post_status','Deleted')->WhereNotNull('time_deleted')->WhereNotNull('deleted_by')->delete();

        return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view_post($id)
    {   
        $get_post = Post::findOrFail($id);
        $post = Post::where('id', $id)->with('user')->get();
        
        return view('admin.postview', compact('post'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete_post($id)
    {
        $get_post = Post::findOrFail($id);
        $post = Post::where('id', $id)->first();
        $session = Auth::user();

        $delete = "Deleted";
        $update = Post::where('id',$id)->update(['post_status' => $delete , 'time_deleted' => now() , 'deleted_by' => $session->id]);
        return redirect()->back();
    }

    public function delete_permanently($id){
        $get_post = Post::findOrFail($id);
        $post = Post::where('id', $id)->first();

        $delete = Post::where('id',$id)->delete();
        return redirect()->back();
    }

    public function approve_from_delete($id){
        $get_post = Post::findOrFail($id);
        $post = Post::where('id', $id)->first();
        $approve = "Approved";

        $approve = Post::where('id',$id)->update(['post_status' => $approve, 'time_approved'=>now(), 'time_deleted'=> null, 'deleted_by' => null]);
        return redirect()->back();
    }

    public function approve_post($id)
    {
        $get_post = Post::findOrFail($id);
        $post = Post::where('id', $id)->first();

        $approve = "Approved";
        $update = Post::where('id',$id)->update(['post_status' => $approve]);
        return redirect()->back();
    }

    
}
